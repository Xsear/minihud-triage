-------------------------------------------------------------------------------
-- MiniDisplay by CookieDuster
-------------------------------------------------------------------------------
-- Inspired by WeaponRange and OmniBar by Hanachi, Claritii by Arkii
-- Uses EventHandler by Lemon King
-- Contains ancient and terrible code (didn't fix what wasn't broken)
-------------------------------------------------------------------------------

require "math";
require "string";
require "table";
require "lib/lib_InterfaceOptions"
require "./lib/lib_EventHandler"
require "lib/lib_Callback2"
require "lib/lib_TextFormat"

local message;
local OnMessageOption = {}

local UI = {
	General = {
		Enabled,
		HudShow = true,
		InterfaceShow = false,
		Scope,
	},
	Player = {
		IsInVehicle = false,
		WeaponInfo,

	},
	Ammo = {
		Frame = Component.GetFrame("miniBARAmmoFrame"),
		Text = Component.GetWidget("miniBARAmmoText"),
		HideWhenFull,
		HideFullDelay,
		HideWhenNotFull,
		HideNotFullDelay,
		HideDelay,
		ShowInVehicle,
		TextColor = {alpha = 0.8, tint = "FFFFFF", exposure = 0},
		TextColorLow = {alpha = 0.8, tint = "FF0000", exposure = 0},
		Glow,
		GlowColor = {alpha = 0.4, tint = "FFFFFF", exposure = 0},
		Low = 0.25,
		Enabled,
		Format,
		InMagazine,
		Reserve,
		Suffix,
		UsesAmmo = true,
	},
	Health = {
		Frame = Component.GetFrame("miniBARHealthFrame"),
		Text = Component.GetWidget("miniBARHealthText"),
		HideWhenFull,
		HideFullDelay,
		ShowInVehicle,
		TextColor = {alpha = 0.8, tint = "64C8FF", exposure = 0},
		TextColorLow = {alpha = 0.8, tint = "FF0000", exposure = 0},
		Glow,
		GlowColor = {alpha = 0.4, tint = "64C8FF", exposure = 0},
		Low = 0.25,
		Enabled,
		Format,
		Suffix,
	},
	Energy = {
		Frame = Component.GetFrame("miniBAREnergyFrame"),
		Text = Component.GetWidget("miniBAREnergyText"),
		HideWhenFull,
		HideFullDelay,
		ShowInVehicle,
		TextColor = {alpha = 0.8, tint = "E1C83C", exposure = 0},
		TextColorLow = {alpha = 0.8, tint = "FF0000", exposure = 0},
		Glow,
		GlowColor = {alpha = 0.4, tint = "E1C83C", exposure = 0},
		Low = 0.25,
		Enabled,
		Format,
		Suffix,
	},
	Hkm = {
		Frame = Component.GetFrame("miniBARHkmFrame"),
		Text = Component.GetWidget("miniBARHkmText"),
		HideWhenZero,
		HideFullDelay,
		ShowInVehicle,
		TextColor = {alpha = 0.8, tint = "FFFFFF", exposure = 0},
		TextColorCharged = {alpha = 0.8, tint = "FF0000", exposure = 0},
		Glow,
		GlowColor = {alpha = 0.4, tint = "FFFFFF", exposure = 0},
		Enabled,
		Format,
		Suffix,
		CustomCharged,
		CustomText,
	},
	Range = {
		Frame = Component.GetFrame("miniBARRangeFrame"),
		Text = Component.GetWidget("miniBARRangeText"),
		TextColor = {alpha = 0.8, tint = "00FF00", exposure = 0},
		TextColorHigh = {alpha = 0.8, tint = "FF0000", exposure = 0},
		Glow,
		GlowColor = {alpha = 0.4, tint = "006400", exposure = 0},
		High = 60,
		Enabled,
		Suffix,
		TooFarText,
		RefreshRate,
		HideWithGrenade,
		ShowWithPlasma,
	},
	Exp = {
		Frame = Component.GetFrame("miniBARExpFrame"),
		Text = Component.GetWidget("miniBARExpText"),
		TextColor = {alpha = 0.8, tint = "00FF00", exposure = 0},
		TextColorHigh = {alpha = 0.8, tint = "FF0000", exposure = 0},
		Glow,
		GlowColor = {alpha = 0.4, tint = "006400", exposure = 0},
		Enabled,
	},
}

local DOT = Component.GetWidget("dot")
local LEVEL_DATA

do -- INTERFACE

InterfaceOptions.NotifyOnDisplay(true)
InterfaceOptions.NotifyOnLoaded(true)
InterfaceOptions.AddMovableFrame({frame = UI.Ammo.Frame, label = "MiniHUD ammo", scalable = true,})
InterfaceOptions.AddMovableFrame({frame = UI.Health.Frame, label = "MiniHUD health", scalable = true,})
InterfaceOptions.AddMovableFrame({frame = UI.Energy.Frame, label = "MiniHUD energy", scalable = true,})
InterfaceOptions.AddMovableFrame({frame = UI.Hkm.Frame, label = "MiniHUD HKM", scalable = true,})
InterfaceOptions.AddMovableFrame({frame = UI.Range.Frame, label = "MiniHUD Range", scalable = true,})
InterfaceOptions.AddMovableFrame({frame = UI.Exp.Frame, label = "MiniHUD Exp", scalable = true,})
InterfaceOptions.AddCheckBox({id="ENABLED", label="Enable addon", default=true})
InterfaceOptions.AddCheckBox({id="REPLACE", label="Use minified 3D Action Bar", default=true})

InterfaceOptions.StartGroup({id="AMMO_ENABLED", label="Enable ammo display", checkbox = true, default=true, subtab = {"Ammo"}})
InterfaceOptions.AddCheckBox({id="AMMO_IN_VEHICLE", label="Enable display in vehicle", default=false, subtab = {"Ammo"}})
InterfaceOptions.AddCheckBox({id="AMMO_HIDE_FULL", label="Hide when full", default=true, subtab = {"Ammo"}})
InterfaceOptions.AddSlider({id="AMMO_HIDE_FULL_DELAY", label="Delay before hiding", default=0.5, min = 0, max = 10, inc = 0.5, suffix = "s", subtab = {"Ammo"}})
InterfaceOptions.AddCheckBox({id="AMMO_HIDE_NOTFULL", label="Hide when not full", default=true, subtab = {"Ammo"}})
InterfaceOptions.AddSlider({id="AMMO_HIDE_NOTFULL_DELAY", label="Delay before hiding", default=10, min = 0, max = 60, inc = 0.5, suffix = "s", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceMenu({id="AMMO_FORMAT", label="Ammo display format", default="AmmoLong", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FORMAT", val="AmmoLong", label="In magazine/Reserve", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FORMAT", val="AmmoShort", label="In magazine", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FORMAT", val="AmmoPercent", label="In magazine (percentage)", subtab = {"Ammo"}})
InterfaceOptions.AddTextInput({id="AMMO_SUFFIX", label="Percentage format suffix", default="%", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceMenu({id="AMMO_FONT", label="Font (scroll this)", default="UbuntuBold_13", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FONT", val="UbuntuBold_10", label="Ubuntu Bold - 10", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FONT", val="UbuntuBold_13", label="Ubuntu Bold - 13", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FONT", val="UbuntuBold_26", label="Ubuntu Bold - 26", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FONT", val="UbuntuMedium_12", label="Ubuntu Medium - 12", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FONT", val="UbuntuMedium_18", label="Ubuntu Medium - 18", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FONT", val="UbuntuMedium_50", label="Ubuntu Medium - 50", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FONT", val="Bold_10", label="Eurostile Bold - 11", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FONT", val="Bold_15", label="Eurostile Bold - 15", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FONT", val="Bold_19", label="Eurostile Bold - 19", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FONT", val="Bold_26", label="Eurostile Bold - 26", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FONT", val="Demi_10", label="Demi - 10", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FONT", val="Demi_15", label="Demi - 15", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FONT", val="Demi_20", label="Demi - 20", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FONT", val="Demi_25", label="Demi - 25", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FONT", val="Demi_30", label="Demi - 30", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FONT", val="Demi_35", label="Demi - 35", subtab = {"Ammo"}})
InterfaceOptions.AddChoiceEntry({menuId="AMMO_FONT", val="Demi_40", label="Demi - 40", subtab = {"Ammo"}})
InterfaceOptions.AddColorPicker({id="AMMO_COLOR", label="Color", default={alpha = 0.8, tint = "FFFFFF", exposure = 0}, subtab = {"Ammo"}})
InterfaceOptions.AddColorPicker({id="AMMO_COLOR_LOW", label="Low ammo color", default={alpha = 0.8, tint = "FF0000", exposure = 0}, subtab = {"Ammo"}})
InterfaceOptions.AddSlider({id="AMMO_LOW", label="Low ammo threshold", default=0.25, min = 0, max = 1, inc = 0.01, suffix = "%", multi = 100, subtab = {"Ammo"}})
InterfaceOptions.AddCheckBox({id="AMMO_GLOW", label="Enable glow", default=true, subtab = {"Ammo"}})
InterfaceOptions.AddColorPicker({id="AMMO_COLOR_GLOW", label="Glow color", default={alpha = 0.4, tint = "003CFF", exposure = 0}, subtab = {"Ammo"}})
InterfaceOptions.StopGroup({subtab = "Ammo"})

InterfaceOptions.StartGroup({id="HEALTH_ENABLED", label="Enable health display", checkbox = true, default=true, subtab = "Health"})
InterfaceOptions.AddCheckBox({id="HEALTH_IN_VEHICLE", label="Enable display in vehicle", default=false, subtab = "Health"})
InterfaceOptions.AddCheckBox({id="HEALTH_HIDE_FULL", label="Hide when full", default=true, subtab = "Health"})
InterfaceOptions.AddSlider({id="HEALTH_HIDE_DELAY", label="Delay before hiding", default=0.5, min = 0, max = 10, inc = 0.5, suffix = "s", subtab = "Health"})
InterfaceOptions.AddChoiceMenu({id="HEALTH_FORMAT", label="Display format", default="HealthShort", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FORMAT", val="HealthLong", label="Current/Max", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FORMAT", val="HealthShort", label="Current", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FORMAT", val="HealthPercent", label="Percentage", subtab = "Health"})
InterfaceOptions.AddTextInput({id="HEALTH_SUFFIX", label="Percentage format suffix", default="%", subtab = "Health"})
InterfaceOptions.AddChoiceMenu({id="HEALTH_FONT", label="Font (scroll this)", default="UbuntuBold_13", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FONT", val="UbuntuBold_10", label="Ubuntu Bold - 10", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FONT", val="UbuntuBold_13", label="Ubuntu Bold - 13", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FONT", val="UbuntuBold_26", label="Ubuntu Bold - 26", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FONT", val="UbuntuMedium_12", label="Ubuntu Medium - 12", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FONT", val="UbuntuMedium_18", label="Ubuntu Medium - 18", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FONT", val="UbuntuMedium_50", label="Ubuntu Medium - 50", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FONT", val="Bold_10", label="Eurostile Bold - 11", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FONT", val="Bold_15", label="Eurostile Bold - 15", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FONT", val="Bold_19", label="Eurostile Bold - 19", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FONT", val="Bold_26", label="Eurostile Bold - 26", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FONT", val="Demi_10", label="Demi - 10", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FONT", val="Demi_15", label="Demi - 15", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FONT", val="Demi_20", label="Demi - 20", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FONT", val="Demi_25", label="Demi - 25", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FONT", val="Demi_30", label="Demi - 30", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FONT", val="Demi_35", label="Demi - 35", subtab = "Health"})
InterfaceOptions.AddChoiceEntry({menuId="HEALTH_FONT", val="Demi_40", label="Demi - 40", subtab = "Health"})
InterfaceOptions.AddColorPicker({id="HEALTH_COLOR", label="Color", default={alpha = 0.8, tint = "64C8FF", exposure = 0}, subtab = "Health"})
InterfaceOptions.AddColorPicker({id="HEALTH_COLOR_LOW", label="Low health color", default={alpha = 0.8, tint = "FF0000", exposure = 0}, subtab = "Health"})
InterfaceOptions.AddSlider({id="HEALTH_LOW", label="Low health threshold", default=0.25, min = 0, max = 1, inc = 0.01, suffix = "%", multi = 100, subtab = "Health"})
InterfaceOptions.AddCheckBox({id="HEALTH_GLOW", label="Enable glow", default=true, subtab = "Health"})
InterfaceOptions.AddColorPicker({id="HEALTH_COLOR_GLOW", label="Glow color", default={alpha = 0.4, tint = "003CFF", exposure = 0}, subtab = "Health"})
InterfaceOptions.StopGroup({subtab = "Health"})

InterfaceOptions.StartGroup({id="ENERGY_ENABLED", label="Enable energy display", checkbox = true, default=true, subtab = "Energy"})
InterfaceOptions.AddCheckBox({id="ENERGY_IN_VEHICLE", label="Enable display in vehicle", default=false, subtab = "Energy"})
InterfaceOptions.AddCheckBox({id="ENERGY_HIDE_FULL", label="Hide when full", default=true, subtab = "Energy"})
InterfaceOptions.AddSlider({id="ENERGY_HIDE_DELAY", label="Delay before hiding", default=0.5, min = 0, max = 10, inc = 0.5, suffix = "s", subtab = "Energy"})
InterfaceOptions.AddChoiceMenu({id="ENERGY_FORMAT", label="Display format", default="EnergyPercent", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FORMAT", val="EnergyLong", label="Current/Max", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FORMAT", val="EnergyShort", label="Current", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FORMAT", val="EnergyPercent", label="Percentage", subtab = "Energy"})
InterfaceOptions.AddTextInput({id="ENERGY_SUFFIX", label="Percentage format suffix", default="%", subtab = "Energy"})
InterfaceOptions.AddChoiceMenu({id="ENERGY_FONT", label="Font (scroll this)", default="UbuntuBold_13", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FONT", val="UbuntuBold_10", label="Ubuntu Bold - 10", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FONT", val="UbuntuBold_13", label="Ubuntu Bold - 13", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FONT", val="UbuntuBold_26", label="Ubuntu Bold - 26", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FONT", val="UbuntuMedium_12", label="Ubuntu Medium - 12", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FONT", val="UbuntuMedium_18", label="Ubuntu Medium - 18", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FONT", val="UbuntuMedium_50", label="Ubuntu Medium - 50", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FONT", val="Bold_10", label="Eurostile Bold - 11", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FONT", val="Bold_15", label="Eurostile Bold - 15", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FONT", val="Bold_19", label="Eurostile Bold - 19", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FONT", val="Bold_26", label="Eurostile Bold - 26", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FONT", val="Demi_10", label="Demi - 10", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FONT", val="Demi_15", label="Demi - 15", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FONT", val="Demi_20", label="Demi - 20", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FONT", val="Demi_25", label="Demi - 25", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FONT", val="Demi_30", label="Demi - 30", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FONT", val="Demi_35", label="Demi - 35", subtab = "Energy"})
InterfaceOptions.AddChoiceEntry({menuId="ENERGY_FONT", val="Demi_40", label="Demi - 40", subtab = "Energy"})
InterfaceOptions.AddColorPicker({id="ENERGY_COLOR", label="Color", default={alpha = 0.8, tint = "E1C83C", exposure = 0}, subtab = "Energy"})
InterfaceOptions.AddColorPicker({id="ENERGY_COLOR_LOW", label="Low energy color", default={alpha = 0.8, tint = "FF0000", exposure = 0}, subtab = "Energy"})
InterfaceOptions.AddSlider({id="ENERGY_LOW", label="Low energy threshold", default=0.25, min = 0, max = 1, inc = 0.01, suffix = "%", multi = 100, subtab = "Energy"})
InterfaceOptions.AddCheckBox({id="ENERGY_GLOW", label="Enable glow", default=true, subtab = "Energy"})
InterfaceOptions.AddColorPicker({id="ENERGY_COLOR_GLOW", label="Glow color", default={alpha = 0.4, tint = "641E00", exposure = 0}, subtab = "Energy"})
InterfaceOptions.StopGroup({subtab = "Energy"})

InterfaceOptions.StartGroup({id="HKM_ENABLED", label="Enable HKM display", checkbox = true, default=true, subtab = "HKM"})
InterfaceOptions.AddCheckBox({id="HKM_IN_VEHICLE", label="Enable display in vehicle", default=false, subtab = "HKM"})
InterfaceOptions.AddCheckBox({id="HKM_HIDE_ZERO", label="Hide when zero", default=true, subtab = "HKM"})
InterfaceOptions.AddSlider({id="HKM_HIDE_DELAY", label="Delay before hiding", default=0.5, min = 0, max = 10, inc = 0.5, suffix = "s", subtab = "HKM"})
InterfaceOptions.AddChoiceMenu({id="HKM_FORMAT", label="Display format", default="HKMPercent", subtab = "HKM"})
--InterfaceOptions.AddChoiceEntry({menuId="HKM_FORMAT", val="HKMLong", label="Current/Max", subtab = "HKM"})
--InterfaceOptions.AddChoiceEntry({menuId="HKM_FORMAT", val="HKMShort", label="Current", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FORMAT", val="HKMPercent", label="Percentage", subtab = "HKM"})
InterfaceOptions.AddTextInput({id="HKM_SUFFIX", label="Percentage format suffix", default="%", subtab = "HKM"})
InterfaceOptions.AddChoiceMenu({id="HKM_FONT", label="Font (scroll this)", default="UbuntuBold_13", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FONT", val="UbuntuBold_10", label="Ubuntu Bold - 10", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FONT", val="UbuntuBold_13", label="Ubuntu Bold - 13", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FONT", val="UbuntuBold_26", label="Ubuntu Bold - 26", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FONT", val="UbuntuMedium_12", label="Ubuntu Medium - 12", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FONT", val="UbuntuMedium_18", label="Ubuntu Medium - 18", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FONT", val="UbuntuMedium_50", label="Ubuntu Medium - 50", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FONT", val="Bold_10", label="Eurostile Bold - 11", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FONT", val="Bold_15", label="Eurostile Bold - 15", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FONT", val="Bold_19", label="Eurostile Bold - 19", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FONT", val="Bold_26", label="Eurostile Bold - 26", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FONT", val="Demi_10", label="Demi - 10", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FONT", val="Demi_15", label="Demi - 15", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FONT", val="Demi_20", label="Demi - 20", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FONT", val="Demi_25", label="Demi - 25", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FONT", val="Demi_30", label="Demi - 30", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FONT", val="Demi_35", label="Demi - 35", subtab = "HKM"})
InterfaceOptions.AddChoiceEntry({menuId="HKM_FONT", val="Demi_40", label="Demi - 40", subtab = "HKM"})
InterfaceOptions.AddColorPicker({id="HKM_COLOR", label="Color", default={alpha = 0.8, tint = "FFFFFF", exposure = 0}, subtab = "HKM"})
InterfaceOptions.AddColorPicker({id="HKM_COLOR_CHARGED", label="100% charge color", default={alpha = 0.8, tint = "FF0000", exposure = 0}, subtab = "HKM"})
InterfaceOptions.AddCheckBox({id="HKM_GLOW", label="Enable glow", default=true, subtab = "HKM"})
InterfaceOptions.AddColorPicker({id="HKM_COLOR_GLOW", label="Glow color", default={alpha = 0.4, tint = "003CFF", exposure = 0}, subtab = "HKM"})
InterfaceOptions.AddCheckBox({id="HKM_CUSTOM_CHARGED", label="Display custom text when charged", default=true, subtab = "HKM"})
InterfaceOptions.AddTextInput({id="HKM_CUSTOM_TEXT", label="Custom text when charged", default="HKM ready!", subtab = "HKM"})
InterfaceOptions.StopGroup({subtab = "HKM"})

InterfaceOptions.StartGroup({id="RANGE_ENABLED", label="Enable range display", checkbox = true, default=true, subtab = "Rangefinder"})
InterfaceOptions.AddTextInput({id="RANGE_SUFFIX", label="Suffix", default="m", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceMenu({id="RANGE_FONT", label="Range font (scroll this)", default="UbuntuBold_13", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceEntry({menuId="RANGE_FONT", val="UbuntuBold_10", label="Ubuntu Bold - 10", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceEntry({menuId="RANGE_FONT", val="UbuntuBold_13", label="Ubuntu Bold - 13", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceEntry({menuId="RANGE_FONT", val="UbuntuBold_26", label="Ubuntu Bold - 26", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceEntry({menuId="RANGE_FONT", val="UbuntuMedium_12", label="Ubuntu Medium - 12", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceEntry({menuId="RANGE_FONT", val="UbuntuMedium_18", label="Ubuntu Medium - 18", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceEntry({menuId="RANGE_FONT", val="UbuntuMedium_50", label="Ubuntu Medium - 50", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceEntry({menuId="RANGE_FONT", val="Bold_10", label="Eurostile Bold - 11", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceEntry({menuId="RANGE_FONT", val="Bold_15", label="Eurostile Bold - 15", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceEntry({menuId="RANGE_FONT", val="Bold_19", label="Eurostile Bold - 19", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceEntry({menuId="RANGE_FONT", val="Bold_26", label="Eurostile Bold - 26", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceEntry({menuId="RANGE_FONT", val="Demi_10", label="Demi - 10", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceEntry({menuId="RANGE_FONT", val="Demi_15", label="Demi - 15", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceEntry({menuId="RANGE_FONT", val="Demi_20", label="Demi - 20", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceEntry({menuId="RANGE_FONT", val="Demi_25", label="Demi - 25", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceEntry({menuId="RANGE_FONT", val="Demi_30", label="Demi - 30", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceEntry({menuId="RANGE_FONT", val="Demi_35", label="Demi - 35", subtab = "Rangefinder"})
InterfaceOptions.AddChoiceEntry({menuId="RANGE_FONT", val="Demi_40", label="Demi - 40", subtab = "Rangefinder"})
InterfaceOptions.AddColorPicker({id="RANGE_COLOR", label="Color", default={alpha = 0.8, tint = "00FF00", exposure = 0}, subtab = "Rangefinder"})
InterfaceOptions.AddColorPicker({id="RANGE_COLOR_HIGH", label="Out of range color", default={alpha = 0.8, tint = "FF0000", exposure = 0}, subtab = "Rangefinder"})
InterfaceOptions.AddSlider({id="RANGE_HIGH", label="Out of range threshold", default=60, min = 0, max = 220, inc = 1, suffix = "m", subtab = "Rangefinder"})
InterfaceOptions.AddTextInput({id="RANGE_HIGH_TEXT", label="Out of measurable range text", default="Out of range", subtab = "Rangefinder"})
InterfaceOptions.AddSlider({id="RANGE_REFRESH", label="Refresh rate (CPU hog!)", default=0.1, min = 0.01, max = 1, inc = 0.01, suffix = "s", subtab = "Rangefinder"})
InterfaceOptions.AddCheckBox({id="RANGE_GLOW", label="Enable glow", default=true, subtab = "Rangefinder"})
InterfaceOptions.AddColorPicker({id="RANGE_COLOR_GLOW", label="Glow color", default={alpha = 0.4, tint = "006400", exposure = 0}, subtab = "Rangefinder"})


InterfaceOptions.AddCheckBox({id="RANGE_HIDE_GRENADE", label="Hide with grenade launchers", default=false, subtab = "Rangefinder"}) -- expand on this

InterfaceOptions.StopGroup({subtab = "Rangefinder"})

InterfaceOptions.StartGroup({id="XP_ENABLED", label="Enable XP display", checkbox = true, default=true, subtab = "Experience"})

InterfaceOptions.AddChoiceMenu({id="XP_FORMAT", label="Format", default="absolute", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FORMAT", val="percentage", label="Percentage", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FORMAT", val="absolute", label="Current/Next", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FORMAT", val="needed", label="Needed for next level", subtab = "Experience"})
InterfaceOptions.AddCheckBox({id="XP_HIDE_MAX", label="Hide when maxed", default=true, subtab = "Experience"})
InterfaceOptions.AddTextInput({id="XP_PREFIX", label="Prefix", default="", subtab = "Experience"})
InterfaceOptions.AddTextInput({id="XP_SUFFIX", label="Suffix", default=" XP", subtab = "Experience"})
InterfaceOptions.AddChoiceMenu({id="XP_FONT", label="Range font (scroll this)", default="Demi_10", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FONT", val="UbuntuBold_10", label="Ubuntu Bold - 10", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FONT", val="UbuntuBold_13", label="Ubuntu Bold - 13", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FONT", val="UbuntuBold_26", label="Ubuntu Bold - 26", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FONT", val="UbuntuMedium_12", label="Ubuntu Medium - 12", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FONT", val="UbuntuMedium_18", label="Ubuntu Medium - 18", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FONT", val="UbuntuMedium_50", label="Ubuntu Medium - 50", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FONT", val="Bold_10", label="Eurostile Bold - 11", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FONT", val="Bold_15", label="Eurostile Bold - 15", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FONT", val="Bold_19", label="Eurostile Bold - 19", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FONT", val="Bold_26", label="Eurostile Bold - 26", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FONT", val="Demi_10", label="Demi - 10", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FONT", val="Demi_15", label="Demi - 15", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FONT", val="Demi_20", label="Demi - 20", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FONT", val="Demi_25", label="Demi - 25", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FONT", val="Demi_30", label="Demi - 30", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FONT", val="Demi_35", label="Demi - 35", subtab = "Experience"})
InterfaceOptions.AddChoiceEntry({menuId="XP_FONT", val="Demi_40", label="Demi - 40", subtab = "Experience"})
InterfaceOptions.AddColorPicker({id="XP_COLOR", label="Color", default={alpha = 0.8, tint = "FFFFFF", exposure = 0}, subtab = "Experience"})
InterfaceOptions.AddCheckBox({id="XP_GLOW", label="Enable glow", default=true, subtab = "Experience"})
InterfaceOptions.AddColorPicker({id="XP_COLOR_GLOW", label="Glow color", default={alpha = 0.4, tint = "003CFF", exposure = 0}, subtab = "Experience"})
InterfaceOptions.StopGroup({subtab = "Experience"})

InterfaceOptions.StartGroup({id="DOT_ENABLED", label="Enable permanent center dot", checkbox = true, default=true, subtab = "Dot"})
InterfaceOptions.AddColorPicker({id="DOT_COLOR", label="Color", default={alpha = 0.8, tint = "00FF00", exposure = 0}, subtab = "Dot"})
InterfaceOptions.AddCheckBox({id="DOT_GLOW", label="Enable glow", default=true, subtab = "Dot"})
InterfaceOptions.AddColorPicker({id="DOT_COLOR_GLOW", label="Glow color", default={alpha = 0.4, tint = "00FF00", exposure = 0}, subtab = "Dot"})
InterfaceOptions.AddCheckBox({id="IS_DOT_FINE", label="Use high precision", default=true, subtab = "Dot"})
InterfaceOptions.AddSlider({id="DOT_SIZE", label="Size", default=4, min=0, max=5.9, inc=0.1, subtab = "Dot"})
InterfaceOptions.AddSlider({id="DOT_LARGE_SIZE", label="Size", default=6, min=6, max=50, inc=1, subtab = "Dot"})
InterfaceOptions.StopGroup({subtab = "Dot"})

end -- end of functions block


do -- FUNCTIONS

function OnComponentLoad()
	InterfaceOptions.SetCallbackFunc(function(id, val)
		OnMessage({type=id, data=val})
	end, "MiniDisplay")

	LEVEL_DATA = Game.GetProgressionUnlocks()
end

function OnMessage(args)
	local option, message = args.type, args.data
	if not ( OnMessageOption[option] ) then log("["..option.."] Not Found") return nil end
	OnMessageOption[option](message)
end

function OnEvent(args)
	EventHandler(args)
end

UI.Health.UpdateVisibility =
	function (message)
		if ((not UI.General.Enabled))
		or (not UI.Health.Enabled)
		or (UI.Player.IsInVehicle and not UI.Health.ShowInVehicle)
		or (not UI.General.HudShow)
		or (UI.General.InterfaceShow) then
			UI.Health.Text:Hide()
		else
			UI.Health.Text:Show()
		end
	end

UI.Ammo.UpdateVisibility =
	function (message)
		if ((not UI.General.Enabled))
		or (not UI.Ammo.Enabled)
		or (UI.Player.IsInVehicle and not UI.Ammo.ShowInVehicle)
		or (not UI.General.HudShow)
		or (UI.General.InterfaceShow) then
			UI.Ammo.Text:Hide()
		else
			UI.Ammo.Text:Show()
		end
	end

UI.Energy.UpdateVisibility =
	function (message)
		if ((not UI.General.Enabled))
		or (not UI.Energy.Enabled)
		or (UI.Player.IsInVehicle and not UI.Energy.ShowInVehicle)
		or (not UI.General.HudShow)
		or (UI.General.InterfaceShow) then
			UI.Energy.Text:Hide()
		else
			UI.Energy.Text:Show()
		end
	end

UI.Hkm.UpdateVisibility =
	function (message)
		if ((not UI.General.Enabled))
		or (not UI.Hkm.Enabled)
		or (UI.Player.IsInVehicle and not UI.Hkm.ShowInVehicle)
		or (not UI.General.HudShow)
		or (UI.General.InterfaceShow) then
			UI.Hkm.Text:Hide()
		else
			UI.Hkm.Text:Show()
		end
	end

UI.Range.UpdateVisibility =
	function (message)
		UI.Player.WeaponInfo = Player.GetWeaponInfo()
		if (UI.Player.WeaponInfo == nil) then
			return
		end
		if ((not UI.General.Enabled))
		or (not UI.Range.Enabled)
		or (UI.Player.IsInVehicle)
		or (not UI.General.HudShow)
		or (UI.General.InterfaceShow)
		then
			UI.Range.Text:Hide()
		else
			UI.Range.Text:Show()
		end
	end

UI.Exp.UpdateVisibility =
	function (message)
		if ((not UI.General.Enabled))
		or (not UI.Exp.Enabled)
		or (not UI.General.HudShow)
		or (UI.General.InterfaceShow)
		or (hide_maxed_xp and LEVEL_DATA[Player.GetLevel()].xp_cost == 0)
		then
			UI.Exp.Text:Hide()
		else
			UI.Exp.Text:Show()
		end
	end

UI.General.UpdateVisibility =
	function (message)
		UI.Ammo.UpdateVisibility()
		UI.Health.UpdateVisibility()
		UI.Energy.UpdateVisibility()
		UI.Hkm.UpdateVisibility()
		UI.Range.UpdateVisibility()
		UI.Exp.UpdateVisibility()
	end

UI.Range.Update =
	function (message)
		local distance = Player.GetReticleInfo().Distance
		local entityInfo = (Player.GetReticleInfo().entityId and Game.GetTargetInfo(Player.GetReticleInfo().entityId) or nil)
		local weaponRange = ((Player.GetWeaponInfo() and Player.GetWeaponInfo().Range) and Player.GetWeaponInfo().Range or nil)
		local TF = TextFormat.Create()

		if (distance > (weaponRange and weaponRange or UI.Range.High) or distance == -1) then
			TF:AppendColor(UI.Range.TextColorHigh.tint)
			UI.Range.Text:SetParam("exposure", UI.Range.TextColorHigh.exposure)
			UI.Range.Text:SetParam("alpha", UI.Range.TextColorHigh.alpha)

		else
			TF:AppendColor(UI.Range.TextColor.tint)
			UI.Range.Text:SetParam("exposure", UI.Range.TextColor.exposure)
			UI.Range.Text:SetParam("alpha", UI.Range.TextColor.alpha)
		end

		if (distance == -1) then
			TF:AppendText(UI.Range.TooFarText)

		else
			TF:AppendText(tostring(math.ceil(distance)) .. UI.Range.Suffix)

			if (entityInfo and entityInfo.name) then
				TF:AppendColor(entityInfo.hostile and Component.LookupColor("hostile") or ((entityInfo.faction and entityInfo.faction == "neutral") and Component.LookupColor("neutral") or Component.LookupColor("friendly")))
				TF:AppendText("\n" .. entityInfo.name)
			end
		end

		TF:ApplyTo(UI.Range.Text)
		Callback2.FireAndForget(UI.Range.Update, param, UI.Range.RefreshRate)
	end
end


do -- OPTIONS (OnMessage)

OnMessageOption.ENABLED =
	function (message)
		UI.General.Enabled = (message == true)
		UI.General.UpdateVisibility()
	end

OnMessageOption.HEALTH_ENABLED =
	function (message)
		UI.Health.Enabled = (message)
		UI.Health.UpdateVisibility()
	end

OnMessageOption.HEALTH_IN_VEHICLE =
	function (message)
		UI.Health.ShowInVehicle = (message)
		UI.Health.UpdateVisibility()
	end

OnMessageOption.HEALTH_FONT =
	function (message)
		local tmp = UI.Health.Text:GetText()
		UI.Health.Text:SetFont(message)
		UI.Health.Text:SetText(tmp)
	end

OnMessageOption.HEALTH_COLOR =
	function (message)
		UI.Health.TextColor = message
	end

OnMessageOption.HEALTH_COLOR_LOW =
	function (message)
		UI.Health.TextColorLow = message
	end

OnMessageOption.HEALTH_HIDE_FULL =
	function (message)
		UI.Health.HideWhenFull = message
		UI.Health.UpdateVisibility()
	end

OnMessageOption.HEALTH_HIDE_DELAY =
	function (message)
		UI.Health.HideFullDelay = message
	end

OnMessageOption.HEALTH_FORMAT =
	function (message)
		UI.Health.Format = message
	end

OnMessageOption.HEALTH_SUFFIX =
	function (message)
		UI.Health.Suffix = message
	end

OnMessageOption.HEALTH_LOW =
	function (message)
		UI.Health.Low = message
	end

OnMessageOption.HEALTH_GLOW =
	function (message)
		UI.Health.Glow = message
	end

OnMessageOption.HEALTH_COLOR_GLOW =
	function (message)
		UI.Health.GlowColor = message
	end

OnMessageOption.AMMO_ENABLED =
	function (message)
		UI.Ammo.Enabled = (message)
		UI.Ammo.UpdateVisibility()
	end

OnMessageOption.AMMO_IN_VEHICLE =
	function (message)
		UI.Ammo.ShowInVehicle = (message)
		UI.Ammo.UpdateVisibility()
	end

OnMessageOption.AMMO_FONT =
	function (message)
		local tmp = UI.Ammo.Text:GetText()
		UI.Ammo.Text:SetFont(message)
		UI.Ammo.Text:SetText(tmp)
	end

OnMessageOption.AMMO_COLOR =
	function (message)
		UI.Ammo.TextColor = message
	end

OnMessageOption.AMMO_COLOR_LOW =
	function (message)
		UI.Ammo.TextColorLow = message
	end

OnMessageOption.AMMO_HIDE_FULL =
	function (message)
		UI.Ammo.HideWhenFull = message
	end

OnMessageOption.AMMO_HIDE_FULL_DELAY =
	function (message)
		UI.Ammo.HideFullDelay = message
	end

OnMessageOption.AMMO_HIDE_NOTFULL =
	function (message)
		UI.Ammo.HideWhenNotFull = message
	end

OnMessageOption.AMMO_HIDE_NOTFULL_DELAY =
	function (message)
		UI.Ammo.HideNotFullDelay = message
	end

OnMessageOption.AMMO_FORMAT =
	function (message)
		UI.Ammo.Format = message
	end

OnMessageOption.AMMO_SUFFIX =
	function (message)
		UI.Ammo.Suffix = message
	end

OnMessageOption.AMMO_LOW =
	function (message)
		UI.Ammo.Low = message
	end

OnMessageOption.AMMO_GLOW =
	function (message)
		UI.Ammo.Glow = message
	end

OnMessageOption.AMMO_COLOR_GLOW =
	function (message)
		UI.Ammo.GlowColor = message
	end


OnMessageOption.ENERGY_ENABLED =
	function (message)
		UI.Energy.Enabled = (message)
		UI.Energy.UpdateVisibility()
	end

OnMessageOption.ENERGY_IN_VEHICLE =
	function (message)
		UI.Energy.ShowInVehicle = (message)
		UI.Energy.UpdateVisibility()
	end

OnMessageOption.ENERGY_FONT =
	function (message)
		local tmp = UI.Energy.Text:GetText()
		UI.Energy.Text:SetFont(message)
		UI.Energy.Text:SetText(tmp)
	end

OnMessageOption.ENERGY_COLOR =
	function (message)
		UI.Energy.TextColor = message
	end

OnMessageOption.ENERGY_COLOR_LOW =
	function (message)
		UI.Energy.TextColorLow = message
	end

OnMessageOption.ENERGY_HIDE_FULL =
	function (message)
		UI.Energy.HideWhenFull = message
	end

OnMessageOption.ENERGY_HIDE_DELAY =
	function (message)
		UI.Energy.HideFullDelay = message
	end

OnMessageOption.ENERGY_FORMAT =
	function (message)
		UI.Energy.Format = message
	end

OnMessageOption.ENERGY_SUFFIX =
	function (message)
		UI.Energy.Suffix = message
	end

OnMessageOption.ENERGY_LOW =
	function (message)
		UI.Energy.Low = message
	end

OnMessageOption.ENERGY_GLOW =
	function (message)
		UI.Energy.Glow = message
	end

OnMessageOption.ENERGY_COLOR_GLOW =
	function (message)
		UI.Energy.GlowColor = message
	end


OnMessageOption.HKM_ENABLED =
	function (message)
		UI.Hkm.Enabled = (message)
		UI.Hkm.UpdateVisibility()
	end

OnMessageOption.HKM_IN_VEHICLE =
	function (message)
		UI.Hkm.ShowInVehicle = (message)
		UI.Hkm.UpdateVisibility()
	end

OnMessageOption.HKM_FONT =
	function (message)
		local tmp = UI.Hkm.Text:GetText()
		UI.Hkm.Text:SetFont(message)
		UI.Hkm.Text:SetText(tmp)
	end

OnMessageOption.HKM_COLOR =
	function (message)
		UI.Hkm.TextColor = message
	end

OnMessageOption.HKM_COLOR_CHARGED =
	function (message)
		UI.Hkm.TextColorCharged = message
	end

OnMessageOption.HKM_HIDE_ZERO =
	function (message)
		UI.Hkm.HideWhenZero = message
		UI.Hkm.UpdateVisibility()
	end

OnMessageOption.HKM_HIDE_DELAY =
	function (message)
		UI.Hkm.HideFullDelay = message
	end

OnMessageOption.HKM_FORMAT =
	function (message)
		UI.Hkm.Format = message
	end

OnMessageOption.HKM_SUFFIX =
	function (message)
		UI.Hkm.Suffix = message
	end

OnMessageOption.HKM_GLOW =
	function (message)
		UI.Hkm.Glow = message
	end

OnMessageOption.HKM_COLOR_GLOW =
	function (message)
		UI.Hkm.GlowColor = message
	end

OnMessageOption.HKM_CUSTOM_CHARGED =
	function (message)
		UI.Hkm.CustomCharged = message
	end

OnMessageOption.HKM_CUSTOM_TEXT =
	function (message)
		UI.Hkm.CustomText = message
	end


OnMessageOption.RANGE_ENABLED =
	function (message)
		UI.Range.Enabled = (message)
		UI.Range.UpdateVisibility()
	end

OnMessageOption.RANGE_FONT =
	function (message)
		local tmp = UI.Range.Text:GetText()
		UI.Range.Text:SetFont(message)
		UI.Range.Text:SetText(tmp)
	end

OnMessageOption.RANGE_COLOR =
	function (message)
		UI.Range.TextColor = message
	end

OnMessageOption.RANGE_COLOR_HIGH =
	function (message)
		UI.Range.TextColorHigh = message
	end

OnMessageOption.RANGE_SUFFIX =
	function (message)
		UI.Range.Suffix = message
	end

OnMessageOption.RANGE_HIGH =
	function (message)
		UI.Range.High = message
	end

OnMessageOption.RANGE_GLOW =
	function (message)
		UI.Range.Glow = message
	end

OnMessageOption.RANGE_COLOR_GLOW =
	function (message)
		UI.Range.GlowColor = message
	end

OnMessageOption.RANGE_HIGH_TEXT =
	function (message)
		UI.Range.TooFarText = message
	end

OnMessageOption.RANGE_HIDE_GRENADE =
	function (message)
		UI.Range.HideGrenade = message
	end

OnMessageOption.RANGE_REFRESH =
	function (message)
		UI.Range.RefreshRate = message
	end

OnMessageOption.XP_ENABLED =
	function (message)
		UI.Exp.Enabled = (message)
		UI.Exp.UpdateVisibility()
	end

OnMessageOption.XP_FONT =
	function (message)
		local tmp = UI.Exp.Text:GetText()
		UI.Exp.Text:SetFont(message)
		UI.Exp.Text:SetText(tmp)
	end

OnMessageOption.XP_FORMAT = function (message)
	xp_format = message
	UpdateXpText()
end

OnMessageOption.XP_COLOR =
	function (message)
		UI.Exp.TextColor = message
		UI.Exp.Text:SetTextColor(UI.Exp.TextColor.tint)
		UI.Exp.Text:SetParam("exposure", UI.Exp.TextColor.exposure)
		UI.Exp.Text:SetParam("alpha", UI.Exp.TextColor.alpha)
	end

OnMessageOption.XP_PREFIX =
	function (message)
		UI.Exp.Prefix = message
		UpdateXpText()
	end


OnMessageOption.XP_SUFFIX =
	function (message)
		UI.Exp.Suffix = message
		UpdateXpText()
	end

OnMessageOption.XP_GLOW =
	function (message)
		UI.Exp.Glow = message
	end

OnMessageOption.XP_COLOR_GLOW =
	function (message)
		UI.Exp.GlowColor = message
	end

OnMessageOption.XP_HIDE_MAX =
	function (message)
		hide_maxed_xp = (message == true)
		UpdateXpText()
	end

OnMessageOption.__DISPLAY =
	function (message)
		Events.ON_WEAPON_CHANGED()
		Events.ON_HEALTH_CHANGED()
		Events.ON_ENERGY_CHANGED()
		Events.ON_SUPERCHARGE_CHANGED({amount=0})
	end

OnMessageOption.__LOADED =
	function (message)
		is_options_init = true
	end

OnMessageOption.REPLACE =
	function (message)
		replace_default = message
	end

OnMessageOption.DOT_ENABLED =
	function (message)
		DOT:Show(message)
	end

OnMessageOption.DOT_COLOR =
	function (message)
		DOT:SetParam("tint", message.tint)
		DOT:SetParam("exposure", message.exposure)
		DOT:SetParam("alpha", message.alpha)
	end

OnMessageOption.DOT_GLOW =
	function (message)
		dot_glow = message
		UpdateDotGlow()
	end

OnMessageOption.DOT_COLOR_GLOW =
	function (message)
		dot_glow_color = message
		UpdateDotGlow()
	end

function UpdateDotGlow()
	if dot_glow and dot_glow_color then
		DOT:SetParam("glow", string.format("%02x", math.floor(dot_glow_color.alpha*255)) .. dot_glow_color.tint)
	else
		DOT:SetParam("glow", 0)
	end
end

OnMessageOption.IS_DOT_FINE =
	function (value)
		is_dot_fine = (value == true)
		InterfaceOptions.EnableOption("DOT_SIZE", value)
		InterfaceOptions.EnableOption("DOT_LARGE_SIZE", not value)
		SetDotSize()
	end

OnMessageOption.DOT_SIZE =
	function (message)
		dot_size = message
		SetDotSize()
	end

OnMessageOption.DOT_LARGE_SIZE =
	function (message)
		dot_large_size = message
		SetDotSize()
	end
end

function SetDotSize()
	local value
	if is_dot_fine == true then
		value = dot_size
	elseif is_dot_fine == false then
		value = dot_large_size
	else
		return
	end

	if not value then
		return
	end
	DOT:SetDims("center-x:50%; center-y:50%; width:"..value.."; height:"..value..";")
end

function Events.ON_PLAYER_READY(args)
	is_player_init = true
	InitStuff()
end

function InitStuff()
	if is_displays_init then return end
	if not is_options_init then
		Callback2.FireAndForget(InitStuff, nil, 1)
		return
	end
	UI.Range.Update()
	UI.Player.WeaponInfo = Player.GetWeaponInfo()
	if (UI.General.Enabled) then
		UI.General.HudShow = true
		UI.General.InterfaceShow = false
		UI.General.UpdateVisibility()
		UpdateXpText()
	end
	--Component.GenerateEvent("MY_CHAT_MESSAGE", {channel="system", text="MiniHUD loaded."})
	Events.ON_WEAPON_CHANGED()
	Events.ON_HEALTH_CHANGED()
	Events.ON_ENERGY_CHANGED()
	Events.ON_SUPERCHARGE_CHANGED({amount=0})
	Events.ON_EXPERIENCE_CHANGED()
	is_displays_init = true
end

function Events.MY_HUD_SHOW(args)
	if(args.reticle_scope == nil) then
		UI.General.HudShow = args.show
	end
	UI.General.UpdateVisibility()
end

function Events.ON_EXPERIENCE_CHANGED(args)
	UpdateXpText()
end

function Events.ON_ELITE_LEVELS_XP_UPDATED(args)
	UpdateXpText()
end

function UpdateXpText()
	local current_xp = Player.GetCurrentProgressionXp()
	if not is_options_init or not current_xp then
		Callback2.FireAndForget(UpdateXpText, nil, 1)
		return
	end
	local current_level = Player.GetLevel()

	local percent = 100
	local cost = LEVEL_DATA[current_level].xp_cost

	-- Elite XP
	if (current_level >= Player.GetMaxLevel()) then
		current_xp	= Player.GetEliteLevelsInfo_XpAndLevel().current_xp
		cost		= Player.GetEliteLevelsInfo_Init().xp_per_level
	end

	local needed = cost - current_xp

	if(cost > 0) then
		percent = math.min(1.0, math.max(0.0, current_xp / cost))
	end

	local text

	if xp_format == "percentage" then
		text = string.format("%d", percent * 100)
	elseif xp_format == "absolute" then
		text = current_xp.." / "..cost
	elseif xp_format == "needed" then
		text = needed
	end

	UI.Exp.Text:SetText(UI.Exp.Prefix..text..UI.Exp.Suffix)
	UI.Exp.UpdateVisibility()
end

function Events.ON_WEAPON_CHANGED()
	if (UI.General.Enabled and UI.Ammo.Enabled) then
		UI.Player.WeaponInfo = Player.GetWeaponInfo()
		if (UI.Player.WeaponInfo == nil) then
			UI.Ammo.UsesAmmo = false
			return
		else
			UI.Ammo.UsesAmmo = true
		end
		--UI.Range.UpdateVisibility()
		if ((UI.Player.WeaponInfo.WeaponType == "Grenade Launcher (secondary)") and UI.Range.HideGrenade) then
			UI.Range.Text:SetParam("alpha", 0)
		else
			UI.Range.Text:SetParam("alpha", UI.Range.TextColor.alpha)
		end
		if (UI.Player.WeaponInfo.AmmoPerBurst > 0) then
			Events.ON_WEAPON_STATE_CHANGED()
		end
	end
end

function Events.ON_WEAPON_STATE_CHANGED(args)
	if (UI.General.Enabled and UI.Ammo.Enabled and UI.Ammo.UsesAmmo) then
		local weapon_state = Player.GetWeaponState(true);
		if (weapon_state and (
				(weapon_state.Clip and weapon_state.Clip ~= UI.Ammo.InMagazine) or
				(weapon_state.Ammo and weapon_state.Ammo ~= UI.Ammo.Reserve))) then -- only execute the rest if the ammo count is changed
			UI.Ammo.Reserve = weapon_state.Ammo
			UI.Ammo.InMagazine = weapon_state.Clip
			if (UI.Ammo.Glow) then
				UI.Ammo.Text:SetParam("glow", string.format("%02x", math.floor(UI.Ammo.GlowColor.alpha*255)) .. UI.Ammo.GlowColor.tint)
			else
				UI.Ammo.Text:SetParam("glow", 0)
			end
			if (weapon_state.Clip < UI.Ammo.Low * UI.Player.WeaponInfo.ClipSize) then
				UI.Ammo.Text:SetTextColor(UI.Ammo.TextColorLow.tint)
				UI.Ammo.Text:SetParam("exposure", UI.Ammo.TextColorLow.exposure)
				UI.Ammo.Text:SetParam("alpha", UI.Ammo.TextColorLow.alpha)
			else
				UI.Ammo.Text:SetTextColor(UI.Ammo.TextColor.tint)
				UI.Ammo.Text:SetParam("exposure", UI.Ammo.TextColor.exposure)
				UI.Ammo.Text:SetParam("alpha", UI.Ammo.TextColor.alpha)
			end
			if (UI.Ammo.Format == "AmmoLong") then
				if (weapon_state.Ammo == math.huge or weapon_state.MaxAmmo == math.huge) then
					UI.Ammo.Text:SetText(weapon_state.Clip .. "/" ..Component.LookupText("INFINITY_SYMBOL"))
				else
					UI.Ammo.Text:SetText(weapon_state.Clip .. "/" ..weapon_state.Ammo)
				end
			elseif (UI.Ammo.Format == "AmmoShort") then
				UI.Ammo.Text:SetText(weapon_state.Clip);
			elseif (UI.Ammo.Format == "AmmoPercent") then
				UI.Ammo.Text:SetText(string.format("%d", (weapon_state.Clip/UI.Player.WeaponInfo.ClipSize*100)) .. UI.Ammo.Suffix);
			end
			if(((UI.Player.WeaponInfo.ClipSize + UI.Player.WeaponInfo.MaxAmmo) == (weapon_state.Clip + weapon_state.Ammo)) and (UI.Player.WeaponInfo.ClipSize == weapon_state.Clip) and UI.Ammo.HideWhenFull) then
				UI.Ammo.Text:ParamTo("alpha", 0, 1, UI.Ammo.HideFullDelay)
			else
				UI.Ammo.Text:ParamTo("alpha", UI.Ammo.TextColor.alpha, 0)
				if (UI.Ammo.HideWhenNotFull) then
					UI.Ammo.Text:ParamTo("alpha", 0, 1, UI.Ammo.HideNotFullDelay)
				end
			end
		end
	end
end

function Events.ON_SEAT_CHANGED(args)
	if (Player.GetAttachmentId()) then
		UI.Player.IsInVehicle = true
	else
		UI.Player.IsInVehicle = false
	end
	UI.General.UpdateVisibility()
end

function Events.ON_HEALTH_CHANGED(args)
	if (UI.General.Enabled and UI.Health.Enabled) and is_player_init then
		local health_state = Player.GetLifeInfo()
		if (UI.Health.Glow) then
			UI.Health.Text:SetParam("glow", string.format("%02x", math.floor(UI.Health.GlowColor.alpha*255)) .. UI.Health.GlowColor.tint)
		else
			UI.Health.Text:SetParam("glow", 0)
		end
		if (health_state.Health < UI.Health.Low * health_state.MaxHealth) then
			UI.Health.Text:SetTextColor(UI.Health.TextColorLow.tint)
			UI.Health.Text:SetParam("exposure", UI.Health.TextColorLow.exposure)
			UI.Health.Text:SetParam("alpha", UI.Health.TextColorLow.alpha)
		else
			UI.Health.Text:SetTextColor(UI.Health.TextColor.tint)
			UI.Health.Text:SetParam("exposure", UI.Health.TextColor.exposure)
			UI.Health.Text:SetParam("alpha", UI.Health.TextColor.alpha)
		end
		if (UI.Health.Format == "HealthLong") then
			UI.Health.Text:SetText(health_state.Health .. "/" ..health_state.MaxHealth);
		elseif (UI.Health.Format == "HealthShort") then
			UI.Health.Text:SetText(health_state.Health);
		elseif (UI.Health.Format == "HealthPercent") then
			UI.Health.Text:SetText(string.format("%d", (health_state.Health/health_state.MaxHealth*100)) .. UI.Health.Suffix);
		end

		if(health_state.Health == health_state.MaxHealth and UI.Health.HideWhenFull) then
			UI.Health.Text:ParamTo("alpha", 0, 1, UI.Health.HideFullDelay)
		else
			UI.Health.Text:ParamTo("alpha", UI.Energy.TextColor.alpha, 0)
		end
	end
end

function Events.ON_ENERGY_CHANGED(args)
	if (UI.General.Enabled and UI.Energy.Enabled) and is_player_init then
		local CurrentEnergy, MaxEnergy, RateEnergy = Player.GetEnergy();
		if(MaxEnergy == 0) then -- avoiding battleframe change quirks
			return
		end
		if (UI.Energy.Glow) then
			UI.Energy.Text:SetParam("glow", string.format("%02x", math.floor(UI.Energy.GlowColor.alpha*255)) .. UI.Energy.GlowColor.tint)
		else
			UI.Energy.Text:SetParam("glow", 0)
		end
		if (CurrentEnergy < UI.Energy.Low * MaxEnergy) then
			UI.Energy.Text:SetTextColor(UI.Energy.TextColorLow.tint)
			UI.Energy.Text:SetParam("exposure", UI.Energy.TextColorLow.exposure)
			UI.Energy.Text:SetParam("alpha", UI.Energy.TextColorLow.alpha)
		else
			UI.Energy.Text:SetTextColor(UI.Energy.TextColor.tint)
			UI.Energy.Text:SetParam("exposure", UI.Energy.TextColor.exposure)
			UI.Energy.Text:SetParam("alpha", UI.Energy.TextColor.alpha)
		end
		if (UI.Energy.Format == "EnergyLong") then
			UI.Energy.Text:SetText(string.format("%d", CurrentEnergy) .. "/" .. string.format("%d", MaxEnergy));
		elseif (UI.Energy.Format == "EnergyShort") then
			UI.Energy.Text:SetText(string.format("%d", CurrentEnergy));
		elseif (UI.Energy.Format == "EnergyPercent") then
			UI.Energy.Text:SetText(string.format("%d", (CurrentEnergy/MaxEnergy*100)) .. UI.Energy.Suffix);
		end
		if(CurrentEnergy > 0.98 * MaxEnergy and UI.Energy.HideWhenFull) then -- energy rounding bug, MaxEnergy != CurrentEnergy even at maximum energy
			UI.Energy.Text:ParamTo("alpha", 0, 1, UI.Energy.HideFullDelay)
		else
			UI.Energy.Text:ParamTo("alpha", UI.Energy.TextColor.alpha, 0)
		end
	end
end

function Events.ON_SUPERCHARGE_CHANGED(args)
	if (UI.General.Enabled and UI.Hkm.Enabled) then
		if (UI.Hkm.Glow) then
			UI.Hkm.Text:SetParam("glow", string.format("%02x", math.floor(UI.Hkm.GlowColor.alpha*255)) .. UI.Hkm.GlowColor.tint)
		else
			UI.Hkm.Text:SetParam("glow", 0)
		end
		if (args.amount == 100) then
			UI.Hkm.Text:SetTextColor(UI.Hkm.TextColorCharged.tint)
			UI.Hkm.Text:SetParam("exposure", UI.Hkm.TextColorCharged.exposure)
			UI.Hkm.Text:SetParam("alpha", UI.Hkm.TextColorCharged.alpha)
		else
			UI.Hkm.Text:SetTextColor(UI.Hkm.TextColor.tint)
			UI.Hkm.Text:SetParam("exposure", UI.Hkm.TextColor.exposure)
			UI.Hkm.Text:SetParam("alpha", UI.Hkm.TextColor.alpha)
		end
		if (UI.Hkm.CustomCharged and args.amount == 100) then
			UI.Hkm.Text:SetText(UI.Hkm.CustomText);
		else
			UI.Hkm.Text:SetText(string.format("%d", args.amount) .. UI.Hkm.Suffix);
		end
		if (args.amount == 0 and UI.Hkm.HideWhenZero) then
			UI.Hkm.Text:ParamTo("alpha", 0, 1, UI.Hkm.HideFullDelay)
		else
			UI.Hkm.Text:ParamTo("alpha", UI.Hkm.TextColor.alpha, 0)
		end
	end
end
