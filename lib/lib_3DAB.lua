-------------------------------------------------------------------------------
--	HUD element removal library by CookieDuster (WIP)
-------------------------------------------------------------------------------
--	Allows to show, hide and completely remove HUD elements
--	Usage:
--	_3DAB.Show(bool) 	- shows 3DAB if bool = true or omitted
--	_3DAB.Hide(bool) 	- hides 3DAB if bool = true or omitted
--	_Glider.Remove()	- removes the glider visuals (irreversible)
--	_XPBar.Remove()		- removes the XP Bar (irreversible)
-------------------------------------------------------------------------------

_3DAB = {}
_Glider = {}
_XPBar = {}

local FOSTER_FRAME = Component.CreateFrame("TrackingFrame", "_hud_foster")
local FOSTER_WIDGET

_3DAB.Show = function(self, show)
	if show == nil then show = true end
	assert(type(show) == "boolean", "Parameter must be boolean")
	Component.GenerateEvent("MY_VEHICLE_MODE_SHOW", {show = not show, fake = true,}) -- show = true to hide 3DAB
end

_3DAB.Hide = function(self, hide)
	if hide == nil then hide = true end
	assert(type(hide) == "boolean", "Parameter must be boolean")
	Component.GenerateEvent("MY_VEHICLE_MODE_SHOW", {show = hide, fake = true,}) -- show = true to hide 3DAB
end

_3DAB.Remove = function(self) -- not supported yet
	--[[ WIP, ignore this
	log("creating frame")
	--FOSTER_FRAME = Component.CreateFrame("OverlayFrame", "_3dab_foster")
	
	
	
	local TARGET_COMPONENT_NAME = "3dActionBar"
	
	FOSTER_MAIN_WIDGET = Component.CreateWidget('<group dimensions="dock:fill;"/>', FOSTER_FRAME)
	Component.FosterWidget(TARGET_COMPONENT_NAME..":Main.{1}", FOSTER_MAIN_WIDGET)
	FOSTER_MAIN_WIDGET:Show(false)
	
	FOSTER_LIST = {
		"CenterConsoleFrame",
		"DurabilityFrame",
		"AmmoLabelFrame",
		"HPLabelFrame",
		"Icon",
		"Icon[1]",
		"Icon[2]",
		"IconBackground",
		"IconBackground[1]",
		"IconBackground[2]",
		"{1}",
		"{2}",
		"{3}",
		"{4}",
		"{5}",
		"{6}",
		"{7}",
		"{8}",
	}
	
	FOSTER_WIDGETS = {}
	
	for i,v in pairs(FOSTER_LIST) do
		for t=1,10 do
			FOSTER_WIDGETS[i] = {}
			log("creating widget")
			FOSTER_WIDGETS[i][t] = Component.CreateWidget('<group dimensions="dock:fill;"/>', FOSTER_FRAME)
			local name = TARGET_COMPONENT_NAME..":"..FOSTER_LIST[i]..".{"..t.."}"
			log("fostering "..name)
			Component.FosterWidget(name, FOSTER_WIDGETS[i][t])
			log("hiding")
			FOSTER_WIDGETS[i][t]:Show(false)
		end
	end
	
	frames:

	background
	iconbackground
	icon
	cooldown
	background
	iconbackground
	icon
	cooldown
	CenterConsoleFrame
	DurabilityFrame

	CenterConsoleFrame:CenterConsole
	DurabilityFrame:DurabilityConsole
	AmmoLabelFrame:AmmoLabel
	HPLabelFrame:HPLabel
	Icon:AbilityIcon
	IconBackground:AbilityIconBackground

	--]]
end

_Glider.Remove = function(self)
	local foster_tree = {
		["Glider"] = {
			["yaw_frame"] = {
				"yaw_art",
				"Warnings",
			},
			["Main"] = {
				"yaw",
				"yaw_bar",
			},
		},
	}
	RemoveTree(foster_tree)
end



_XPBar.Remove = function(self)
	local foster_tree = {
		["XPBar"] = {
			["XPBar"] = {
				"XP",
			},
		},
	}
	RemoveTree(foster_tree)
end

local function RemoveTree(tree, level, path)
	local FosterFrame = Component.CreateFrame("TrackingFrame")
	if not level then level = 1 end
	if not path then path = "" end
	if not tree[1] then -- not the last level
		for name,branch in pairs(tree) do
			if level == 1 then
				RemoveTree(branch, level+1, path..name..":")
			else
				RemoveTree(branch, level+1, path..name..".")
			end
		end
	else -- last level
		for i,v in pairs(tree) do
			--log("level: "..level)
			log("not removing: "..tostring(path)..tostring(v))
			--local FosterWidget = Component.CreateWidget('<group dimensions="dock:fill;"/>', FosterFrame)
			--Component.FosterWidget(path, FosterWidget)
			--FosterWidget:Show(false)
			--Component.RemoveWidget(FosterWidget)
		end
	end
	Component.RemoveFrame(FosterFrame)
end
