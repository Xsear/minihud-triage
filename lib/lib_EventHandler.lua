-- Event Manager
-- XML Should be bound directly to EventHandler

Events = setmetatable({},  {__index = function(self, key) log(key.." has no bound Event Function!") end})	-- Events are bound here

function EventHandler(args)
	local EventId = string.upper(args.event)

	if Events[EventId] then
		Events[EventId](args)
	end
end
