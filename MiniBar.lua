--------------------------------------------------------------------
-- 3D Action Bar - Abilities only mod by Whraith
--------------------------------------------------------------------
-- Based entirely on 3D Action Bar by Red5
-- Fixes and addon version by CookieDuster
--------------------------------------------------------------------

require "unicode"
require "math"
require "lib/lib_Colors"
require "lib/lib_Debug"
require "lib/lib_InterfaceOptions"
require "lib/lib_StringMarkUp";
require "lib/lib_ToolTip";
require "lib/lib_Attract";
require "lib/lib_HudManager";
require "lib/lib_ProgressBar";
require "lib/lib_Items"
require "lib/lib_Shader"
require "lib/lib_ItemCard"
require "lib/lib_InputIcon"
require "./lib/lib_3DAB"

Debug.EnableLogging(false);


local g_BaseAnchor = nil;
local BASE_ANCHOR_TILT = 20; -- tilt on the entire UI to achieve desired look

local g_LEFT_WING = {};
local g_RIGHT_WING = {};

local ABI_ICON_OFFSET = 0;
local ABI_ICON_INCREMENT = 110;
local BRIGHTIFY_SCALE = 1.0; -- multiplied by model element colors to get extra brightness
local MESH_DETAIL_COLOR = "#727272";


local NAME_BACKGROUND_PADDING = 10;

local COOLDOWN_ARC_COLOR = "#ff8800";
local HKM_ARC_COLOR = "#02b4e9";
local FAILURE_ARC_COLOR = "#DD0000";

local g_LEFT_DETAIL = nil;
local g_RIGHT_DETAIL = nil;
local g_BACKGROUND_MODEL = nil;

local g_SceneObjectsToRemove = {}; -- this holds all of the scene objects we've loaded, so we can unload them

local g_SuperBarCharge = 0;
local cb_FlashSuperBar = nil;
local cb_PositionDeployables = nil;

local g_SUPERBAR_BG_FRAME = nil;
local g_SUPERBAR_FRAME = nil;
local g_SUPERBAR_SWEEPER = nil;

local TFL_BACKGROUND = 1;
local TFL_ICON_BACKGROUND = 2;
local TFL_ICON = 3;
local TFL_COOLDOWN = 4;

local TFL_NUM_LAYERS = 4;

local TFL_OFFSETS = {0.0, -0.2, -0.4, -0.6};
local TFL_NAMES = {"background", "iconbackground", "icon", "cooldown"};

local ABILITIES_PER_FRAME = 4;

local FIRST_ABILITY_SLOT = 1;
local LAST_ABILITY_SLOT = 4;
local FIRST_CONSUMABLE_SLOT = 5;
local NUM_CONSUMABLE_SLOTS = 4;
local ACTIONBAR_DRAG_ORIGIN = "3dactionbar"; -- loaded into drag/drop data so we know it started with us
local INVALID_ABILITY_ID = "0";

local g_selectedIdx = 1;
local g_tooltipIdx = 0;
local g_abilityInfo = {};
local g_queuedAbilitySwap = {};
local g_calldownSettings = {};
local g_hiddenByHUDHiding = false;
local g_hiddenBecauseVehicle = false;
local g_muted = false;
local g_HKMIndex = nil;
local g_playerGUID = nil;

local cb_UnslotCheckInfo = {};

local g_cooldownFillsArc = false;

local COOLDOWN_COUNTER_SHOW_SECONDS = 4000;
local SLOT_CLOSE_DUR = 0.4;
local ABILITY_ENABLE_DUR = 0.1;
local ABILITY_FAILURE_DUR = 0.1;
local ABILITY_NAME_SHOW_DUR = 4;
local ACTIONBAR_ATTRACTOR_SCALE = 0.6;

local BASE_ANCHOR_POS = {x=0,y=0.9,z=2};
local ONSCREEN_BASE_ANCHOR_POS = BASE_ANCHOR_POS;
local OFFSCREEN_BASE_ANCHOR_POS = {x=0,y=1.4,z=2};
local HIDE_SHOW_DUR = 0.4;
local BASICALLY_ONE_HUNDRED = 99.999;

local anchor_x = 0
local anchor_y = 0.9
local anchor_z = 2

local tilt = 20
local rotation_x = 1
local rotation_y = 0
local rotation_z = 0
local scale_x = 0.05
local scale_y = 0.05
local scale_z = 0.05



local w_Slots = {}; -- legacy name, holds array of the ability slots (currently whatever we have in the ability icon layer)

-- mouse mode hint stuff
local MAIN_GROUP            = Component.GetWidget("MainGroup");
local MOUSE_MODE_HINTS		= {GROUP=Component.GetWidget("MouseModeHints")};
local MOUSE_MODE_EXITHINT   = MOUSE_MODE_HINTS.GROUP:GetChild("MouseModeExitHint");

local g_CENTER_CONSOLE_FRAME = nil;
local g_CENTER_CONSOLE = nil;
local g_CENTER_CONSOLE_GROUP = nil;
local g_DEPLOYABLES_LIST = nil;
local g_DURABILITY_HOLDER = nil;
local g_DURABILITY_FRAME = nil;

local DURABILITY_FRAME_EXPANDED_SCALE = {x=1.4,y=1.4,z=1.4};
local DURABILITY_FRAME_COLLAPSED_SCALE = {x=0.7,y=0.7,z=0.7};




local ITEM_COOLDOWN_GRP = nil
local MED_CARD = nil
local AUX_CARD = nil
local MED_KEY = nil
local AUX_KEY = nil
local ITEMWIDGETS = {}
local c_TiltAngle = 25
local c_KeyFadeTime = 0.1


local g_InputMode = "game"
local cb_FlashMouseModeHint = nil;
local g_ShowMouseModeTips = false;


local HINT_FADE_IN_DELAY_SECONDS = 0.5;
local HINT_FADE_OUT_DELAY_SECONDS = 0.5;
local HINT_SHOW_DELAY_SECONDS = 2;
local HINT_HIDE_DELAY_SECONDS = 0;

local show_selector = false

local AbilityIndex = {
	7, 8, 9, 6
}

ANCHOR_FRAME = Component.GetFrame("AnchorFrame")
InterfaceOptions.NotifyOnDisplay(true)
InterfaceOptions.AddCheckBox({id="DEBUG_MODE", label="Enable debug mode", default=false})
InterfaceOptions.AddCheckBox({id="SHOW_MOUSE_MODE_TIPS", label_key="SHOW_MOUSE_MODE_TIPS", default=true})

InterfaceOptions.AddCheckBox({id="SHOW_SELECTOR", label="Show ability selector", default=true})

InterfaceOptions.StartGroup({id="anchor", label="Anchor position"})
InterfaceOptions.AddTextInput({id="TILT", label="Tilt", default=20})
InterfaceOptions.AddTextInput({id="ANCHOR_X", label="Anchor X (default: 0)", default=0})
InterfaceOptions.AddTextInput({id="ANCHOR_Y", label="Anchor Y (default: 0.9)", default=0.9})
InterfaceOptions.AddTextInput({id="ANCHOR_Z", label="Anchor Z (default: 2)", default=2})
InterfaceOptions.AddTextInput({id="ROTATION_X", label="Rotation X (default: 1)", default=1})
InterfaceOptions.AddTextInput({id="ROTATION_Y", label="Rotation Y (default: 0)", default=0})
InterfaceOptions.AddTextInput({id="ROTATION_Z", label="Rotation Z (default: 0)", default=0})
InterfaceOptions.StopGroup()


function OnComponentLoad()
	InterfaceOptions.SetCallbackFunc(function(id, val) OnMessage({type=id, data=val}) end, "MiniBar")

	local onScreen = true;

	g_BaseAnchor = Component.CreateAnchor("3dActionBarAnchor");
	g_BaseAnchor:SetScene("world");

	if (onScreen) then
		g_BaseAnchor:SetParam("rotation", {axis={x=1,y=0,z=0}, angle=BASE_ANCHOR_TILT});
		g_BaseAnchor:SetParam("scale", {x=0.05,y=0.05,z=0.05});
		g_BaseAnchor:SetParam("translation", ONSCREEN_BASE_ANCHOR_POS);
		g_BaseAnchor:BindToScreen();
	else
		g_BaseAnchor:SetParam("translation", BASE_ANCHOR_POS);
	end

	g_LEFT_WING.FRAMES = {};
	for i = 1, TFL_NUM_LAYERS do
		g_LEFT_WING.FRAMES[i] = {};
		g_LEFT_WING.FRAMES[i].FRAME = CreateTrackingFrame(TFL_NAMES[i]);
		g_LEFT_WING.FRAMES[i].FRAME:SetParam("cullalpha",1);
		if (InteractableLayer(i)) then
			g_LEFT_WING.FRAMES[i].FRAME:SetInteractable(true);
		end

		if ( i == 1 ) then
			g_LEFT_WING.FRAMES[i].FRAME_ANCHOR = g_LEFT_WING.FRAMES[i].FRAME:GetAnchor();
			g_LEFT_WING.FRAMES[i].FRAME_ANCHOR:SetParent(g_BaseAnchor);
			g_LEFT_WING.FRAMES[i].FRAME_ANCHOR:SetParam("translation", {x=-6.9,y=-14,z=1.7});
		else
			g_LEFT_WING.FRAMES[i].FRAME_ANCHOR = g_LEFT_WING.FRAMES[i].FRAME:GetAnchor();
			g_LEFT_WING.FRAMES[i].FRAME_ANCHOR:SetParent(g_LEFT_WING.FRAMES[1].FRAME_ANCHOR);
			g_LEFT_WING.FRAMES[i].FRAME_ANCHOR:SetParam("translation", {x=0,y=TFL_OFFSETS[i],z=0});
		end
	end

	g_RIGHT_WING.FRAMES = {};
	for i = 1, TFL_NUM_LAYERS do
		g_RIGHT_WING.FRAMES[i] = {};
		g_RIGHT_WING.FRAMES[i].FRAME = CreateTrackingFrame(TFL_NAMES[i]);
		g_RIGHT_WING.FRAMES[i].FRAME:SetParam("cullalpha",1);
		if (InteractableLayer(i)) then
			g_RIGHT_WING.FRAMES[i].FRAME:SetInteractable(true);
		end

		if ( i == 1 ) then
			g_RIGHT_WING.FRAMES[i].FRAME_ANCHOR = g_RIGHT_WING.FRAMES[i].FRAME:GetAnchor();
			g_RIGHT_WING.FRAMES[i].FRAME_ANCHOR:SetParent(g_BaseAnchor);
			g_RIGHT_WING.FRAMES[i].FRAME_ANCHOR:SetParam("translation", {x=0.0,y=-14,z=1.7});
		else
			g_RIGHT_WING.FRAMES[i].FRAME_ANCHOR = g_RIGHT_WING.FRAMES[i].FRAME:GetAnchor();
			g_RIGHT_WING.FRAMES[i].FRAME_ANCHOR:SetParent(g_RIGHT_WING.FRAMES[1].FRAME_ANCHOR);
			g_RIGHT_WING.FRAMES[i].FRAME_ANCHOR:SetParam("translation", {x=0,y=TFL_OFFSETS[i],z=0});
		end
	end


	-- Icons

	g_LEFT_WING.FRAMES[TFL_ICON].ICONS = {}
	g_RIGHT_WING.FRAMES[TFL_ICON].ICONS = {}

	for i = 1, ABILITIES_PER_FRAME do
		CreateAbilityIcon(i, i, g_LEFT_WING.FRAMES[TFL_ICON]);
	end

	for i = 1, ABILITIES_PER_FRAME do
		CreateAbilityIcon(i, i + ABILITIES_PER_FRAME, g_RIGHT_WING.FRAMES[TFL_ICON]);
	end

	-- Icon Backgrounds

	g_LEFT_WING.FRAMES[TFL_ICON_BACKGROUND].ICONBACKGROUNDS = {}
	for i = 1, ABILITIES_PER_FRAME do
		g_LEFT_WING.FRAMES[TFL_ICON_BACKGROUND].ICONBACKGROUNDS[i] = Component.CreateWidget("AbilityIconBackground", g_LEFT_WING.FRAMES[TFL_ICON_BACKGROUND].FRAME);
		local left = ABI_ICON_OFFSET + i * ABI_ICON_INCREMENT;
		g_LEFT_WING.FRAMES[TFL_ICON_BACKGROUND].ICONBACKGROUNDS[i]:SetDims("left:"..tostring(left));
	end

	g_RIGHT_WING.FRAMES[TFL_ICON_BACKGROUND].ICONBACKGROUNDS = {}
	for i = 1, ABILITIES_PER_FRAME do
		g_RIGHT_WING.FRAMES[TFL_ICON_BACKGROUND].ICONBACKGROUNDS[i] = Component.CreateWidget("AbilityIconBackground", g_RIGHT_WING.FRAMES[TFL_ICON_BACKGROUND].FRAME);
		local left = ABI_ICON_OFFSET + i * ABI_ICON_INCREMENT;
		g_RIGHT_WING.FRAMES[TFL_ICON_BACKGROUND].ICONBACKGROUNDS[i]:SetDims("left:"..tostring(left));
	end

	g_BACKGROUND_MODEL = CreateSceneObject("ActionBar_Background", "3d_background"); --ActionBar_Background , 3d_background
	local BG_MODEL_SCALE = {x=2.5, y=2.5, z=2.5};
	local bgModelAnchor = g_BACKGROUND_MODEL:GetAnchor();
	bgModelAnchor:SetParent(g_BaseAnchor);
	bgModelAnchor:SetParam("scale", BG_MODEL_SCALE);
	g_BACKGROUND_MODEL:Show(false)

	-- bars


	SetupSimpleModel(g_LEFT_DETAIL_MODEL, bgModelAnchor, MESH_DETAIL_COLOR);
	SetupSimpleModel(g_RIGHT_DETAIL_MODEL, bgModelAnchor, MESH_DETAIL_COLOR);



	-- some of these meshes need a flipped version also
	local flippingRequired = true;

	if (flippingRequired) then
	end


	-- labels

	-- because these have to line up with the 3d mesh, anchoring them off in space


	local centralHKM = false;

	if (centralHKM) then
		g_SUPERBAR_BG_FRAME = CreateTrackingFrame("SuperbarBgFrame");
		g_SUPERBAR_BG_FRAME:GetAnchor():SetParent(bgModelAnchor);
		g_SUPERBAR_BG_FRAME:GetAnchor():SetParam("translation", {x=0,y=.7,z=0.55});

		local superbar_bg = Component.CreateWidget("hkm_background", g_SUPERBAR_BG_FRAME);

		g_SUPERBAR_FRAME = CreateTrackingFrame("SuperbarFrame");
		g_SUPERBAR_FRAME:GetAnchor():SetParent(bgModelAnchor);
		g_SUPERBAR_FRAME:GetAnchor():SetParam("translation", {x=0,y=0.65,z=0.55});
		g_SUPERBAR_FRAME:SetParam("cullalpha",1);

		g_SUPERBAR = {GROUP=Component.CreateWidget("hkm", g_SUPERBAR_FRAME)};
		g_SUPERBAR.HOLDER = g_SUPERBAR.GROUP:GetChild("holder");
		g_SUPERBAR.ARC = g_SUPERBAR.HOLDER:GetChild("hkm_arc");
	end

	-- hotkey in MouseMode
	MSO = LibStringMarkUp.Create( Component.LookupText("MOUSE_MODE_EXIT_HINT") );
	MSO:ApplyTextMarkers( MOUSE_MODE_EXITHINT );

	-- hide hint text
	MOUSE_MODE_HINTS.GROUP:Show(false);

	-- center console action
	g_CENTER_CONSOLE_FRAME = CreateTrackingFrame("CenterConsoleFrame");
	g_CENTER_CONSOLE_FRAME:GetAnchor():SetParent(bgModelAnchor);
	g_CENTER_CONSOLE_FRAME:GetAnchor():SetParam("scale", {x=.7,y=.7,z=.7});
	g_CENTER_CONSOLE_FRAME:SetParam("cullalpha", 1);
	g_CENTER_CONSOLE = Component.CreateWidget("CenterConsole", g_CENTER_CONSOLE_FRAME);

	g_DURABILITY_FRAME = CreateTrackingFrame("DurabilityFrame");
	g_DURABILITY_FRAME:GetAnchor():SetParent(bgModelAnchor);
	g_DURABILITY_FRAME:GetAnchor():SetParam("translation", {x=-0.18,y=0.6,z=0});
	g_DURABILITY_FRAME:GetAnchor():SetParam("scale", DURABILITY_FRAME_EXPANDED_SCALE);
	g_DURABILITY_FRAME:SetParam("cullalpha", 1);
	g_DURABILITY_HOLDER = Component.CreateWidget("DurabilityConsole", g_DURABILITY_FRAME);

	g_CENTER_CONSOLE_GROUP = g_CENTER_CONSOLE:GetChild("CenterConsoleGroup");
	g_DEPLOYABLES_LIST = g_CENTER_CONSOLE_GROUP:GetChild("DeployableList");



	CreateItemCooldownFrame(g_LEFT_WING.FRAMES[1].FRAME_ANCHOR)



	HudManager.BlacklistReasons({"reticle_scope"})
	HudManager.BindOnShow(OnHudShow)
end

function OnComponentUnload()

end

function InteractableLayer(layerId)
	return (layerId == TFL_ICON);
end

function CreateAbilityIcon(localIndex, globalIndex, parent)
	local icon = {ICON=Component.CreateWidget("AbilityIcon", parent.FRAME)};

	icon.HOLDER = icon.ICON:GetChild("holder");
	icon.ICONHOLDER = icon.HOLDER:GetChild("icons");
	icon.NAMEHOLDER = icon.HOLDER:GetChild("abilityname_holder");
	icon.FOCUS = icon.HOLDER:GetChild("focus");
	icon.DROPTARGET = icon.FOCUS:GetChild("droptarget");
	icon.localIndex = localIndex;
	icon.globalIndex = globalIndex;
	icon.CONSUMABLE_COUNTER = icon.HOLDER:GetChild("consumable_counter");
	icon.SELECTION = icon.ICONHOLDER:GetChild("selection");
	icon.COOLDOWN_COUNTER = icon.ICONHOLDER:GetChild("cooldown_counter");
	icon.STATE_COUNTER = icon.ICONHOLDER:GetChild("state_counter");
	icon.COOLDOWN_ARC = icon.ICONHOLDER:GetChild("cooldown");
	icon.COOLDOWN_BP = icon.ICONHOLDER:GetChild("cooldown_bp");
	icon.HKM_ARC_BP = icon.ICONHOLDER:GetChild("hkm_arc_bp");
	icon.HKM_FAIL_BP = icon.ICONHOLDER:GetChild("hkm_fail_bp");
	icon.HKM_ARC = icon.ICONHOLDER:GetChild("hkm_arc");
	icon.HKM_GLOW = icon.ICONHOLDER:GetChild("hkm_glow");
	icon.SCANLINE = icon.ICONHOLDER:GetChild("scanline");
	icon.ACTIVATION = icon.ICONHOLDER:GetChild("activation");
	icon.ACTIVATION2 = icon.ICONHOLDER:GetChild("activation2");
	icon.ITEM_CIRCLE = icon.ICONHOLDER:GetChild("Circle");
	icon.AHB			= icon.HOLDER:GetChild("attractHookBack");
	icon.AHF			= icon.HOLDER:GetChild("attractHookFront");
	icon.LOCK 			= icon.HOLDER:GetChild("lock_info")
	icon.DEPLOY_LIST 	= icon.HOLDER:GetChild("deployable_list")

	icon.LOCK:GetChild("lock_text"):SetText("LVL " .. Game.RequiredLevelForSlot(AbilityIndex[localIndex]))

	if (not IsConsumableSlot(globalIndex)) then
		icon.ITEM_CIRCLE:Show(false);
	end

	icon.hideables = {};
	table.insert(icon.hideables, icon.ICONHOLDER);
	table.insert(icon.hideables, icon.NAMEHOLDER);
	table.insert(icon.hideables, icon.CONSUMABLE_COUNTER);
	table.insert(icon.hideables, icon.FAILURE);

	icon.hkmOnlyParts = {};
	table.insert(icon.hkmOnlyParts, icon.HKM_ARC_BP);
	table.insert(icon.hkmOnlyParts, icon.HKM_FAIL_BP);
	table.insert(icon.hkmOnlyParts, icon.HKM_ARC);
	table.insert(icon.hkmOnlyParts, icon.HKM_GLOW);

	SetupAbilityIconFocus(icon, globalIndex);
	SetupAbilityIconDropTarget(icon, globalIndex);

	parent.ICONS[localIndex] = icon;
	w_Slots[globalIndex] = icon;
	local left = ABI_ICON_OFFSET + localIndex * ABI_ICON_INCREMENT;
	icon.ICON:SetDims("left:"..tostring(left));
end

function SetupAbilityIconFocus(icon, globalIndex)
	local focus = icon.FOCUS;
	local consumable = IsConsumableSlot(globalIndex);

	focus:BindEvent("OnMouseEnter", function()
			local tooltip = FormTooltipForSlot(globalIndex);
			if (tooltip) then
				Tooltip.Show(tooltip, {halign = "left"});
			end
			g_tooltipIdx = globalIndex;
		end );

	focus:BindEvent("OnMouseLeave", function()
			Tooltip.Show(nil);
			g_tooltipIdx = 0;
		end);

	if (consumable) then
		focus:BindEvent("OnMouseDown", function()
				if (not SlotEmpty(globalIndex)) then
					Component.BeginDragDrop("item_sdb_id", GetDragInfoForSlot(globalIndex), nil);
				else
					-- open inventory to calldowns tab
					Component.GenerateEvent("MY_INVENTORY_SHOW", {tab = "calldowns" });
				end
			end);
	else
		focus:BindEvent("OnMouseDown", function()
				if (not SlotEmpty(globalIndex)) then
					Component.BeginDragDrop("ability_id", GetDragInfoForSlot(globalIndex), nil);
				else
					-- anything? should we drop the hand rollover cursor in that case?
				end
			end);
	end

	focus:SetCursor("sys_hand");
end

function OnDragDropEnd(args)
	if (args and args.canceled and args.dragdata and type(args.dragdata) == "string") then
		local dragdata = jsontotable(args.dragdata);

		if (dragdata and dragdata.index and dragdata.from == ACTIONBAR_DRAG_ORIGIN) then
			local dragFailStartIndex = dragdata.index;
			if (IsConsumableSlot(dragFailStartIndex)) then
				local consumableIndex = ConsumableIndexFromGlobal(dragdata.index);
				ClearConsumableSlot(consumableIndex);
			end
		end
	end
end

-- in case we need it someday, it'd go in as parameter 3, "DragListener", to the Component.BeginDragDrop() calls
function DragListener(args)
end

function GetDragInfoForSlot(index)
	local dragData = {};

	if (IsConsumableSlot(index)) then
		dragData = GetDragDataForItemSlot(index);
	else
		dragData = GetDragDataForAbilitySlot(index);
	end

	return dragData;
end

function GetDragDataForAbilitySlot(index)
	-- for ability we want ability ID and can fetch the rest from there i suppose
	return tostring({abilityId = g_abilityInfo[index].abilityId, index = index, from = ACTIONBAR_DRAG_ORIGIN});
end

function GetDragDataForItemSlot(index)
	-- for dragging consumable slots we need the item sdbid, index, and if it's local (to swap rather than replace)
	return tostring({index = index, itemSdbId = g_abilityInfo[index].itemInfo.itemTypeId, from = ACTIONBAR_DRAG_ORIGIN});
end

function SetupAbilityIconDropTarget(icon, globalIndex)
	local dropTarget = icon.DROPTARGET;
	local consumable = IsConsumableSlot(globalIndex);

	if (consumable) then
		dropTarget:SetAcceptTypes("item_sdb_id");
		dropTarget:BindEvent("OnDragDrop", function(args)
			local dropInfoString = dropTarget:GetDropInfo();
			local dropInfo = jsontotable(dropInfoString);

			if (dropInfo) then
				if (dropInfo.from == ACTIONBAR_DRAG_ORIGIN) then
					-- from action bar to action bar, for this type... check indices and swap if they're both valid consumable slots
					SwapConsumables(globalIndex, dropInfo.index);
				else -- probably inventory? maybe elsewhere?
					TryConsumableDrop(dropInfo.itemSdbId, globalIndex);
				end
			end

			end);

		-- PSTODO: we may want these for feedback?
		dropTarget:BindEvent("OnDragEnter", function(args)
			end);
		dropTarget:BindEvent("OnDragLeave", function()
			end);
	else
		dropTarget:SetAcceptTypes("ability_id");
		dropTarget:BindEvent("OnDragDrop", function(args)
			local dropInfo = dropTarget:GetDropInfo();
			local dropInfoTable = jsontotable(dropInfo);

			local draggedSlot = dropInfoTable.index;
			local droppedOnSlot = icon.globalIndex;

			if (draggedSlot and droppedOnSlot and draggedSlot ~= droppedOnSlot and not(IsSlotLocked(droppedOnSlot))) then
				Debug.Log("3dAb sending swap of "..tostring(draggedSlot)..", "..tostring(droppedOnSlot));
				System.SwapLocalCharacterAbilities(draggedSlot, droppedOnSlot);
			end

			end);
		dropTarget:BindEvent("OnDragEnter", function(args)
			end);
		dropTarget:BindEvent("OnDragLeave", function(args)
			end);
	end
end

function UpdatePlayerInfo()
	local name, faction, race, sex, id = Player.GetInfo();

	if (id) then
		g_playerGUID = tostring(id);
	end
end

function OnPlayerReady()
	-- currently just for slotted calldown settings
	UpdatePlayerInfo();
	LoadSettings();

	OnAbilitiesChanged();
	UpdateConsumableCounter();

	OnKeybindingsChange();
--	OnVitalsChanged();
--	OnWeaponStateChanged();
	OnAbilitySelected();

	-- PSTODO: don't like that this will necessarily trigger a wave of OnAbilitiesChanged().
	SlotPendingCalldowns();

	LoadItemCards()
	UpdateKeyBindings()
end

local cb_AbilitiesChanged = nil;
local w_MISSION_ITEM = nil

function OnFakeItemUpdate(args) -- TODO: Add fancy animations.
	if not w_MISSION_ITEM then
		InitializeMissionItem()
	end

	if args.added then
		local itemInfo = Game.GetItemInfoByType(args.item_sdb_id)
		w_MISSION_ITEM.ICON:SetUrl(itemInfo.web_icon)

		w_MISSION_ITEM.HOLDER:ParamTo("alpha", 1, .15, "smooth")
	else
		w_MISSION_ITEM.HOLDER:ParamTo("alpha", 0, .15, "smooth")
	end
end

function InitializeMissionItem()
	w_MISSION_ITEM = {GROUP=Component.CreateWidget("MissionItem", g_CENTER_CONSOLE_GROUP)}

	w_MISSION_ITEM.HOLDER = w_MISSION_ITEM.GROUP:GetChild("holder")
	w_MISSION_ITEM.ICON = w_MISSION_ITEM.HOLDER:GetChild("web_icon")
end

function OnAbilitiesChanged()
	if (cb_AbilitiesChanged ~= nil) then
		cancel_callback(cb_AbilitiesChanged);
	end

	cb_AbilitiesChanged = callback(AbilitiesChanged, nil, 0.05);
end

function AbilitiesChanged()
	cb_AbilitiesChanged = nil;

	if (Player.AbilityUpdatesLocked() or not Player.IsReady()) then
		-- try again later
		callback(OnAbilitiesChanged, nil, 1);
		return;
	end
	local allAbilities = Player.GetAbilities();
	local abilities = allAbilities.slotted;

	SetHKMUnknown();

	LoadItemCards()

	-- hackery. do this nicely somehow later
	for i = 1, NUM_CONSUMABLE_SLOTS do
		abilities[i + FirstConsumableIndex() - 1] = allAbilities["action"..tostring(i)];
	end

	for i = 1, LastConsumableIndex() do
		if (abilities[i] and abilities[i].abilityId) then
			w_Slots[i].LOCK:ParamTo("alpha", 0, SLOT_CLOSE_DUR*2, "smooth")

			CheckAbilityForHKM(abilities[i], i);

			Debug.Log("Abilities["..tostring(i).."]: "..tostring(abilities[i]));

			ShowAbilityInfo(i);
			if (ShouldQueueSwap(i, abilities[i].abilityId)) then
				-- queue a slot for loading
				QueueAbilitySwap(i, abilities[i]);
				AbilityInfo_Update(i);
				Slot_RefreshCooldown(w_Slots[i]);
			elseif (ShouldUpdateAbility(i, abilities[i].abilityId)) then
				-- cooldown change from server can trigger OnAbilityChanged() while ability is not ready and is still not ready,
				-- in that case OnAbilityReady() won't be called to update cooldown
				-- client handles cooldown change in:
				-- tfLocalCharacter::CombatController::Handle_AbilityCooldowns()

				-- reload ability information when cooldown is different
				g_queuedAbilitySwap[i] = abilities[i];
				Slot_LoadAbility(i);
				g_queuedAbilitySwap[i] = nil;
			else -- same ability id, same cooldown
				AbilityInfo_Update(i);
				Slot_RefreshCooldown(w_Slots[i]);
			end
		else
			if (i >= 1 and i <= 4) then
				Debug.Log("Abilities["..tostring(i).."]: empty");
				local isLocked = IsSlotLocked(i)
				w_Slots[i].LOCK:ParamTo("alpha", 1, SLOT_CLOSE_DUR*2, "smooth")
				if isLocked then
					w_Slots[i].ICONHOLDER:SetDims("width:0%; height:0%")
				end
			end
			ClearAbilityInfo(i);
			HideAbilityInfo(i);

			-- hide tooltip if active
			if(i == g_tooltipIdx) then
				Tooltip.Show(nil);
			end
		end
	end
end

function ShouldQueueSwap(globalIndex, abilityId)
	-- we want this swap if:
	-- - there is no swap queued, and this isn't the current ability, or
	-- - there is a swap queued, but it's for something else
	local swapUncontested = g_queuedAbilitySwap[globalIndex] == nil and (g_abilityInfo[globalIndex] == nil or g_abilityInfo[globalIndex].abilityId ~= abilityId);
	local stompPreviousQueuedSwap = (g_queuedAbilitySwap[globalIndex] ~= nil and g_queuedAbilitySwap[globalIndex].abilityId ~= abilityId);

	local result = swapUncontested or stompPreviousQueuedSwap;
	return result;
end

function QueueAbilitySwap(globalIndex, abilityInfo)
	g_queuedAbilitySwap[globalIndex] = abilityInfo;

	-- this is an approved swap. if there was one in progress, we've decided to kill it
	local SLOT = w_Slots[globalIndex];

	if (SLOT.cbOnSwap) then
		-- still working, but on the wrong id; abort!
		cancel_callback(SLOT.cbOnSwap);
		SLOT.cbOnSwap = nil;
	end

	Slot_OnSwap(SLOT);
end

function ShouldUpdateAbility(globalIndex, abilityId)
	-- we want to update the action bar if the ability is the same, but has its cooldown value changed
	local abilityInfo = Player.GetAbilityInfo(abilityId);
	local sameAbility = g_queuedAbilitySwap[globalIndex] == nil and (g_abilityInfo[globalIndex] ~= nil and g_abilityInfo[globalIndex].abilityId == abilityId);
	local infoChanged = false;
	if(sameAbility and abilityInfo) then
		infoChanged = g_abilityInfo[globalIndex].cooldown ~= abilityInfo.cooldown;
	end

	return infoChanged;
end

-- depends on:
--  - slot having its open/close set
function Slot_OnSwap(SLOT)
	SLOT.cbOnSwap = nil;
	if (SLOT.open) then
		-- finish cooldown
		if (SLOT.cb_ExitCooldown) then
			execute_callback(SLOT.cb_ExitCooldown);
		end
		if (IsConsumableSlot(SLOT.globalIndex)) then
			-- instant swap for consumables
			Slot_Close(SLOT, 0);
			Slot_OnSwap(SLOT);
		else
			-- if open, try again when closed
			Slot_Close(SLOT, SLOT_CLOSE_DUR);
			SLOT.cbOnSwap = callback(Slot_OnSwap, SLOT, SLOT_CLOSE_DUR);
		end
	else
		-- swap content
		Slot_LoadAbility(SLOT.globalIndex);
		g_queuedAbilitySwap[SLOT.globalIndex] = nil;
		Slot_Open(SLOT, SLOT_CLOSE_DUR); -- either way? original did this only for non-empty loads

		if (SLOT.globalIndex == g_selectedIdx) then
			OnAbilitySelected();
		end
	end
end

function OnBattleframeChanged()
end

function Slot_Open(SLOT, dur)
	-- optional: could animate name/description as in original
	SLOT.ICONHOLDER:MoveTo("width:100%; height:100%", dur);
	SLOT.open = true;
end

function Slot_Close(SLOT, dur)
	-- optional: could animate name/description as in original
	SLOT.ICONHOLDER:MoveTo("width:0%; height:0%", dur);
	SLOT.open = false;
end

function Slot_LoadAbility(slotGlobalIndex)
	local SLOT = w_Slots[slotGlobalIndex];
	local abilityIconHolder = SLOT.HOLDER;
	local loadInfo = g_queuedAbilitySwap[slotGlobalIndex];
	Debug.Log("["..tostring(slotGlobalIndex).."] getting ability info for "..tostring(loadInfo.abilityId));
	local abiInfo = Player.GetAbilityInfo(loadInfo.abilityId);
	local itemInfo = {};

	if (not abiInfo) then
		Debug.Log("Player.GetAbilityInfo() came up empty with param "..tostring(loadInfo.abilityId));
		return;
	end

	if (loadInfo.itemId) then
		itemInfo = Player.GetItemInfo(loadInfo.itemId);
		loadInfo.itemInfo = itemInfo;
		abiInfo.itemInfo = itemInfo;
	elseif (loadInfo.itemTypeId) then
		itemInfo = Game.GetItemInfoByType(loadInfo.itemTypeId);
		loadInfo.itemInfo = itemInfo;
		abiInfo.itemInfo = itemInfo;
	end

	-- PSTODO: should store these someplace
	local abilityNameHolder = SLOT.NAMEHOLDER;
	local abilityNameField = abilityNameHolder:GetChild("abilityname");
	local abilityNameBackground = abilityNameHolder:GetChild("name_bg");

	if (abiInfo.name) then
		abilityNameField:SetText(abiInfo.name);
	end

	local textDims = abilityNameField:GetTextDims();
	local backgroundWidth = textDims.width + NAME_BACKGROUND_PADDING;
	local backgroundHeight = textDims.height + NAME_BACKGROUND_PADDING;

	abilityNameBackground:MoveTo("width:"..tostring(backgroundWidth).."; height:"..tostring(backgroundHeight).."; center-x:50%; center-y:50%", ABILITY_NAME_FADE_IN_DUR);

	if (abiInfo.itemInfo) then
		abiInfo.description = abiInfo.itemInfo.description;
	end

	StoreAbilityInfo(slotGlobalIndex, abiInfo);
	AbilityInfo_Update(slotGlobalIndex);
	Slot_RefreshCooldown(SLOT);
	CheckAbilityEnabled(slotGlobalIndex);

	local iconGroup = SLOT.ICONHOLDER;

	if (IsConsumableSlot(slotGlobalIndex)) then
		iconGroup:GetChild("icon"):Show(true);
		if (loadInfo and loadInfo.itemInfo) then
			iconGroup:GetChild("icon"):SetIcon(loadInfo.itemInfo.web_icon_id);
		end
		UpdateCalldownSettings(ConsumableIndexFromGlobal(slotGlobalIndex), loadInfo.itemInfo.itemTypeId);
		UpdateConsumableCounter();

		if g_SlottingTutorialStep == c_SlotConsumable then
			Game.SendUIMessageToEncounter(g_TutorialEncounterID, "Slotted", tostring({success = true}))
			g_SlottingTutorialStep = c_UseConsumable

			--let the inventory know it can end its portion of this tutorial
			Component.GenerateEvent("NPE_SLOTTING_TUTORIAL_END", {})
		end
	else
		Debug.Log("["..tostring(slotGlobalIndex).."] setting icon based on id "..tostring(abiInfo.iconId));
		iconGroup:GetChild("icon"):Show(true);
		iconGroup:GetChild("icon"):SetIcon(abiInfo.iconId);
	end

	if (IsHKMSlot(slotGlobalIndex)) then
		ShowHKMParts(SLOT);
		if (IsHKMReady()) then
			HKMEnabled();
		else
			HKMDisabled();
		end
	else
		HideHKMParts(SLOT);
	end
end

function ShowAbilityInfo(slotGlobalIndex)
	local abilityGroup = w_Slots[slotGlobalIndex];

	for i = 1, #abilityGroup.hideables do
		abilityGroup.hideables[i]:Show(true);
	end
end

function HideAbilityInfo(slotGlobalIndex)
	local abilityGroup = w_Slots[slotGlobalIndex];

	for i = 1, #abilityGroup.hideables do
		abilityGroup.hideables[i]:Show(false);
	end
end

function ShowHKMParts(SLOT)
	local numHKMParts = #SLOT.hkmOnlyParts;

	for i = 1, numHKMParts do
		SLOT.hkmOnlyParts[i]:Show(true);
	end
end

function HideHKMParts(SLOT)
	local numHKMParts = #SLOT.hkmOnlyParts;

	for i = 1, numHKMParts do
		SLOT.hkmOnlyParts[i]:Show(false);
	end
end

function CheckAbilityEnabled(slotGlobalIndex)
	local abilityId = g_abilityInfo[slotGlobalIndex].abilityId;
	local abilityState = Player.GetAbilityState(abilityId);

	if (abilityState.isReady) then
		AbilityEnabled(slotGlobalIndex);
	else
		AbilityDisabled(slotGlobalIndex);
	end
end

function AbilityEnabled(slotGlobalIndex)
	local icon = GetIconForAbilitySlot(slotGlobalIndex);
	icon:ParamTo("saturation", 1, ABILITY_ENABLE_DUR);
	icon:ParamTo("alpha", "1", ABILITY_ENABLE_DUR);
	icon:MoveTo("width:100%; height:100%", ABILITY_ENABLE_DUR);

	if (IsConsumableSlot(slotGlobalIndex)) then
		local ItemCircle = w_Slots[slotGlobalIndex].ITEM_CIRCLE;
		local ItemAmount = w_Slots[slotGlobalIndex].CONSUMABLE_COUNTER;
		ItemCircle:MoveTo("width:130%; height:130%", ABILITY_ENABLE_DUR);
		ItemCircle:ParamTo("alpha", "1", ABILITY_ENABLE_DUR);
		ItemAmount:ParamTo("alpha", "1", ABILITY_ENABLE_DUR);
	end

	local GLOW = w_Slots[slotGlobalIndex].HKM_GLOW;
	GLOW:Show(true);
	GLOW:SetParam("tint", "#ffffff");
	GLOW:MoveTo("width:150%;height:150%;",0);
	GLOW:SetParam("alpha", 0.5, 0);
	GLOW:QueueParam("alpha", .0, .15, 0.2);
end

function AbilityDisabled(slotGlobalIndex)
	local icon = GetIconForAbilitySlot(slotGlobalIndex);
	icon:ParamTo("saturation", 0, ABILITY_ENABLE_DUR);
	icon:ParamTo("alpha", "0.3", ABILITY_ENABLE_DUR);

	if (IsHKMSlot(slotGlobalIndex)) then
		icon:MoveTo("width:85%; height:85%", ABILITY_ENABLE_DUR);
	else
		icon:MoveTo("width:70%; height:70%", ABILITY_ENABLE_DUR);
	end

	if (IsConsumableSlot(slotGlobalIndex)) then
		local ItemCircle = w_Slots[slotGlobalIndex].ITEM_CIRCLE;
		local ItemAmount = w_Slots[slotGlobalIndex].CONSUMABLE_COUNTER;
		ItemCircle:MoveTo("width:90%; height:90%", ABILITY_ENABLE_DUR);
		ItemCircle:ParamTo("alpha", "0.3", ABILITY_ENABLE_DUR);
		ItemAmount:ParamTo("alpha", "0", ABILITY_ENABLE_DUR);
	end

	local GLOW = w_Slots[slotGlobalIndex].HKM_GLOW;
	GLOW:Show(false);
end

function HKMEnabled()
	if (g_HKMIndex) then
		AbilityEnabled(g_HKMIndex);
		local GLOW = w_Slots[g_HKMIndex].HKM_GLOW;
		GLOW:Show(true);
		GLOW:SetParam("tint", "#02B4E9");
		GLOW:CycleParam("alpha", 1, 0.5, 0);
		GLOW:MoveTo("width:180%;height:180%;",0);
	end
end

function HKMDisabled()
	if (g_HKMIndex) then
		AbilityDisabled(g_HKMIndex);
		local GLOW = w_Slots[g_HKMIndex].HKM_GLOW;
		GLOW:Show(false);
	end
end

function GetIconForAbilitySlot(slotGlobalIndex)
	local icon = nil;

	if (IsConsumableSlot(slotGlobalIndex)) then
		icon = w_Slots[slotGlobalIndex].ICONHOLDER:GetChild("web_icon");
	else
		icon = w_Slots[slotGlobalIndex].ICONHOLDER:GetChild("icon");
	end

	return icon;
end

local cb_MoveDeployableStack = {}

function OnAbilitySelected()
	local ABILITY_NAME_FADE_OUT_DUR = 0.1;
	local ABILITY_NAME_FADE_IN_DUR = 0.2;
	local newIndex = Player.GetSelectedAbility();

	if (newIndex ~= g_selectedIdx) then
		if (IsValidSlotIndex(newIndex)) then
			local oldNameCalloutHolder = nil;

			if (g_selectedIdx) then
				oldNameCalloutHolder = GetNameCalloutHolder(g_selectedIdx);
				oldNameCalloutHolder:ParamTo("alpha", 0, ABILITY_NAME_FADE_OUT_DUR);
			end

			for idx,SLOT in pairs(w_Slots) do
				if cb_MoveDeployableStack[idx] then cancel_callback(cb_MoveDeployableStack[idx]) cb_MoveDeployableStack[idx] = nil end

				cb_MoveDeployableStack[idx] = callback(function()
					cb_MoveDeployableStack[idx] = nil

					SLOT.DEPLOY_LIST:FinishMove()
					SLOT.DEPLOY_LIST:MoveTo("bottom:-10; height:_", .15, "smooth")
				end, nil, ABILITY_NAME_SHOW_DUR*1.1)

				SLOT.DEPLOY_LIST:FinishMove()
				SLOT.DEPLOY_LIST:MoveTo("bottom:-35; height:_", .15, "smooth")
			end

			local newNameCalloutHolder = GetNameCalloutHolder(newIndex);
			newNameCalloutHolder:ParamTo("alpha", 0.75, ABILITY_NAME_FADE_IN_DUR);
			newNameCalloutHolder:QueueParam("alpha", 0, ABILITY_NAME_FADE_IN_DUR, ABILITY_NAME_SHOW_DUR);

			-- warn("ability select; show: "..tostring(show_selector))
			w_Slots[g_selectedIdx].SELECTION:Show(false);
			w_Slots[newIndex].SELECTION:Show(show_selector);

			g_selectedIdx = newIndex;
		else
			-- hide selection for everyone?
		end
	else
		w_Slots[newIndex].SELECTION:Show(show_selector);
	end

	PlaySound("Play_UI_Ability_Selection");
end

function OnKeybindingsChange()
	local keybinds = System.GetKeyBindings("Combat", false);
	local fireAbility = keybinds["FireAbility"][1];
	for i=1, #w_Slots do
		local SLOT = w_Slots[i].ICON;
		local shortcut = keybinds["SelectAbility"..(i)][1];
		Slot_SetHotkeys(SLOT, fireAbility, shortcut, i);
	end
	UpdateKeyBindings()
end

function OnVitalsChanged()

end

function OnEnergyChanged()

end

function OnTookHit(args)

end

function OnWeaponBurst()

end

function OnWeaponStateChanged()

end

function HKMAngle(fillPercent)
	return 180 - fillPercent / 100 * 360 ;
end

function OnSuperChargeChanged(args)
	local amount = args.amount;
	local duration = args.duration;

	local angle = HKMAngle(amount);

	if (g_HKMIndex and w_Slots[g_HKMIndex]) then
		local HKM_slot = w_Slots[g_HKMIndex];
		ARC = HKM_slot.HKM_ARC;

		ARC:Show(amount > 0);
		ARC:ParamTo("end-angle", angle, duration);

		-- args = {amount, duration}
		local params = {
			increasing = {
				tint = "#02B4E9",
				alpha = 0.9,
				exposure = .4,
			},
			decreasing = {
				tint = "#02B4E9",
				alpha = 0.8,
				exposure = 0,
			},
			full = {
				tint = "#02B4E9",
				alpha = 1,
				exposure = .6,
			},
		}

		local mode;
		if args.amount >= BASICALLY_ONE_HUNDRED and g_SuperBarCharge < BASICALLY_ONE_HUNDRED then
			mode = "full"
		elseif g_SuperBarCharge < args.amount then
			mode = "increasing"
		elseif g_SuperBarCharge > args.amount then
			mode = "decreasing"
		end

		if (mode == "full") then
			HKMEnabled();
		else
			HKMDisabled();
		end

		if mode then
			if mode == "full" then
				cb_FlashSuperBar = callback(FlashSuperBar, nil, duration)
			elseif cb_FlashSuperBar then
				cancel_callback(cb_FlashSuperBar)
				cb_FlashSuperBar = nil
			end

			for param, value in pairs(params[mode]) do
				ARC:ParamTo(param, value, duration, 0, "ease-in");
			end
			g_SuperBarCharge = args.amount
		end
	end
end

function FlashSuperBar()
	cb_FlashSuperBar = nil
	local dur = 2

	if (g_HKMIndex) then
		HKM_ARC = w_Slots[g_HKMIndex].HKM_ARC;
		HKM_ARC:ParamTo("alpha", 0.2, dur/2, 0, "ease-in")
		HKM_ARC:QueueParam("alpha", 0.5, dur/2, 0, "ease-out")
		cb_FlashSuperBar = callback(FlashSuperBar, nil, dur)
	end
end

function OnAbilityUsed(args)

	if args.index ~= -1 then--Then the ability is on the hotbar, and is not the auxiliary or med system

		local abilityId = args.id;
		local abilityIndices = AbilityIndicesFromId(abilityId);

		if (g_abilityInfo) then
			for i = 1, #abilityIndices do
				local abilityIndex = abilityIndices[i];

				local abInfo = g_abilityInfo[abilityIndex];
				if (abInfo) then
					local SLOT = w_Slots[abilityIndex];
					AbilityInfo_Update(abilityIndex);

					if (SLOT) then
						Slot_FlashActivation(SLOT);
						Slot_RefreshCooldown(SLOT);
						OnAbilitySelected();
					end
					PlaySound("Play_UI_Ability_Trigger");
				end
			end
		end
	else
		UpdateItemCooldownDisplay()
	end
end

function OnAbilityReady(arg)
	local abilityId = arg.id;
	local abilityIndices = AbilityIndicesFromId(abilityId);
	local abilityReady = arg.ready;

	if (g_abilityInfo) then
		for i = 1, #abilityIndices do
			local abilityIndex = abilityIndices[i];

			if (abilityReady) then
				AbilityEnabled(abilityIndex);
			else
				AbilityDisabled(abilityIndex);
			end

			local abInfo = g_abilityInfo[abilityIndex];
			if (abInfo) then
				local SLOT = w_Slots[abilityIndex];
				if (SLOT) then
					AbilityInfo_Update(abilityIndex);
					Slot_RefreshCooldown(SLOT);
				end
			end
		end
	end
end

function OnAbilityInEffect(args)
	Debug.Event(args)
end

function OnAbilityState(args) -- args = {state, id, state_dur_total}
	Debug.Event(args)
	local affectedIndices = AbilityIndicesFromId(args.id);
	local numAbilities = #affectedIndices;

	for i = 1, numAbilities do
		local SLOT = w_Slots[affectedIndices[i]];

		if (args.state_dur_total) then
			SLOT.STATE_COUNTER:ParamTo("alpha", 1, 0.1);
			SLOT.STATE_COUNTER:StartTimer(args.state_dur_total, true);
			SLOT.STATE_COUNTER:QueueParam("alpha", 0, 0.1, args.state_dur_total);
		end
	end
end


function DoAbilityFailure(globalSlotIndex)
	local SLOT = w_Slots[globalSlotIndex];

	local failMeter = nil;
	local backToColor = nil;


	if (IsHKMSlot(globalSlotIndex)) then
		failMeter = SLOT.HKM_FAIL_BP;
		failMeter:Show(true);
		backToColor = HKM_ARC_COLOR;
		failMeter:ParamTo("alpha", 1, 0);
		failMeter:QueueParam("alpha", 0, 0.2, 0.05);
		--failMeter:QueueParam("alpha", 1, 0, 0.05);
		--failMeter:QueueParam("alpha", 0, 0.2, 0.01);

		hkmMeter = SLOT.HKM_ARC;
		--hkmMeter:ParamTo("tint", FAILURE_ARC_COLOR, ABILITY_FAILURE_DUR);
		--hkmMeter:QueueParam("tint", HKM_ARC_COLOR, ABILITY_FAILURE_DUR, ABILITY_FAILURE_DUR);
	else
		failMeter = SLOT.COOLDOWN_ARC;
		backToColor = COOLDOWN_ARC_COLOR;
		failMeter:ParamTo("tint", FAILURE_ARC_COLOR, 0);
		failMeter:QueueParam("tint", backToColor, 0, 0.05);
		failMeter:QueueParam("tint", FAILURE_ARC_COLOR, 0, 0.05);
		failMeter:QueueParam("tint", backToColor, 0.2, 0.1);
	end

end

function OnAbilityFailed(args)
	if (not args.index or args.index < 0 or args.index > LastConsumableIndex() or w_Slots[args.index] == nil) then
		return;
	end
	OnAbilitySelected();

	local failedAbilityIndices = AbilityIndicesFromId(args.id);

	for i = 1, #failedAbilityIndices do
		DoAbilityFailure(failedAbilityIndices[i]);
	end

	if args.error then
		Component.GenerateEvent("MY_NOTIFY", {text=args.error});
	end
	PlaySound("Play_SFX_UI_SIN_CooldownFail");
end


function OnDeployableEvent(args)
	-- args = {fosterMe, abilityId}

	if (args.fosterMe) then
		local slotIdx = AbilityIndicesFromId(args.abilityId)
		if slotIdx and w_Slots[slotIdx[1]] then
			Component.FosterWidget(args.fosterMe, w_Slots[slotIdx[1]].DEPLOY_LIST, "full");
		end
	end

	if (cb_PositionDeployables) then
		cancel_callback(cb_PositionDeployables);
	end

	cb_PositionDeployables = callback(PositionDeployables, nil, 0.01);
end

function PositionDeployables()
	cb_PositionDeployables = nil;

	-- for all of these events, just recalculate position based on # of foster chirren
	for _,SLOT in pairs(w_Slots) do
		local newHeight = SLOT.DEPLOY_LIST:GetFosterChildCount() * 25;
		SLOT.DEPLOY_LIST:MoveTo("width:_; left:_; bottom:_; height:"..tostring(newHeight), 0.5);
	end
end

function OnDurabilityFoster(args)
	if (args and args.fosterMe) then
		Component.FosterWidget(args.fosterMe, g_DURABILITY_HOLDER, "full");
	end
end

function OnDurabilityExpand(args)
	local dur = 0.4;

	if (args.expand) then
		g_DURABILITY_HOLDER:MoveTo("width:_; height:_; left:_; bottom:-200", dur);
		g_DURABILITY_FRAME:GetAnchor():ParamTo("scale", DURABILITY_FRAME_EXPANDED_SCALE, dur);
	else
		g_DURABILITY_HOLDER:MoveTo("width:_; height:_; left:_; bottom:205", dur);
		g_DURABILITY_FRAME:GetAnchor():ParamTo("scale", DURABILITY_FRAME_COLLAPSED_SCALE, dur);
	end
end

function OnCacheInventoryChanged()
	UpdateConsumableCounter();
end

function QueueEmptyConsumableUnslot(globalSlotIndex, itemTypeId)
	if (cb_UnslotCheckInfo[globalSlotIndex]) then
		cancel_callback(cb_UnslotCheckInfo[globalSlotIndex].callback);
	end

	local callbackRecord = {
		itemTypeId = itemTypeId,
		callback = callback(ProcessQueuedConsumableUnslot, globalSlotIndex, 1.0)
	};

	cb_UnslotCheckInfo[globalSlotIndex] = callbackRecord;
end

function ProcessQueuedConsumableUnslot(globalSlotIndex)
	local info = g_abilityInfo[globalSlotIndex];

	if (info and info.itemInfo and info.itemInfo.itemTypeId == cb_UnslotCheckInfo[globalSlotIndex].itemTypeId) then
		--[[
		ok, so now that the dust has settled we want to unslot now if:
		- we've got the same item in the same slot we're supposed to be checking, and
		- the count of that item is 0, and
		- it's not 'is_permanent'
		--]]

		local itemCount = Player.GetItemCount(info.itemInfo.itemTypeId);
		local is_permanent = info.itemInfo.flags.is_permanent;

		if (itemCount < 1 and not is_permanent) then
			ClearConsumableSlot(ConsumableIndexFromGlobal(globalSlotIndex));
		end
	end
	cb_UnslotCheckInfo[globalSlotIndex] = nil;
end

function UpdateConsumableCounter()
	-- update the item counters
	for i = FirstConsumableIndex(), LastConsumableIndex() do
		local SLOT = w_Slots[i];
		local info = g_abilityInfo[i];

		if info then
			local itemCount = Player.GetItemCount(info.itemInfo.itemTypeId);
			local is_permanent = info.itemInfo.flags.is_permanent;

			if itemCount then
				if (is_permanent) then
					SLOT.CONSUMABLE_COUNTER:SetText( "" );
				else
					SLOT.CONSUMABLE_COUNTER:SetText( itemCount );
				end

				if (itemCount < 1 and not is_permanent) then
					--[[
					This is an unfortunate workaround. Remove when missions no longer grant 0 of something
					(which later becomes a useful number). This work is just in support of FB 40773 where
					a mission is granting 0 of an item, trying to slot it (fail) and so removing it from the bar
					before the quantity comes in.
					--]]
					QueueEmptyConsumableUnslot(i, info.itemInfo.itemTypeId);
				end
			else
				SLOT.CONSUMABLE_COUNTER:SetText( "" );
			end
		else
			SLOT.CONSUMABLE_COUNTER:SetText( "" );
		end
	end
end

function Slot_SetHotkeys(SLOT, fireAbility, shortcut, index)
	local keybindGroup = SLOT:GetChild("holder"):GetChild("keybind_holder");

	if (shortcut) then
		keybindGroup:Show(true);
		local keybind = keybindGroup:GetChild("keybind");
		keybind:SetText(System.GetKeycodeString(shortcut.keycode));

		local kbBackground = keybindGroup:GetChild("kb_bg");

		local textDims = keybind:GetTextDims();
		local backgroundWidth = textDims.width + NAME_BACKGROUND_PADDING;
		local backgroundHeight = textDims.height + NAME_BACKGROUND_PADDING;

		kbBackground:MoveTo("center-x:_; center-y:_; width:"..tostring(backgroundWidth).."; height:"..tostring(backgroundHeight), ABILITY_NAME_FADE_IN_DUR);
	else
		keybindGroup:Show(false);
	end
end

function GetNameCalloutHolder(i)
	return w_Slots[i].NAMEHOLDER;
end

function StoreAbilityInfo(idx, abiInfo)
	g_abilityInfo[idx] = abiInfo;
end

function ClearAbilityInfo(idx)
	g_abilityInfo[idx] = nil;
end

function AbilityIndicesFromId(abilityId)
	Debug.Table("AbilityIndicesFromId()", abilityId)
	local matchedIndices = {};

	for i = 1, LastConsumableIndex() do
		if (g_abilityInfo[i] and tonumber(g_abilityInfo[i].abilityId) == tonumber(abilityId)) then
			table.insert(matchedIndices, i);
		end
	end

	return matchedIndices;
end

function AbilityInfoFromId(abilityId)
	for i = 1, #g_abilityInfo do
		if (g_abilityInfo[i] and tonumber(g_abilityInfo[i].abilityId) == tonumber(abilityId)) then
			return g_abilityInfo[i];
		end
	end

	return nil;
end

function ItemSdbIdFromSlotIndex(index)
	if (g_abilityInfo and g_abilityInfo[index] and g_abilityInfo[index].itemInfo) then
		return g_abilityInfo[index].itemInfo.itemTypeId;
	else
		return nil;
	end
end

function AbilityInfo_Update(globalIndex)
	local abInfo = g_abilityInfo[globalIndex];

	if (abInfo) then
		abInfo.state = Player.GetAbilityState(abInfo.abilityId);
	end

	-- reload tooltip on ability info update
	if(globalIndex == g_tooltipIdx) then
		local tooltip = FormTooltipForSlot(globalIndex);
		if(tooltip) then
			Tooltip.Show(tooltip, {halign = "left"});
		end
	end
end

function Slot_FlashActivation(SLOT)
	SLOT.ACTIVATION:QueueParam("alpha", 1.0, 0.0);
	SLOT.ACTIVATION:QueueParam("alpha", 0, .3, 0.1,"ease-in");
	SLOT.ACTIVATION:QueueMove("width:100;height:50", 0, 0);
	SLOT.ACTIVATION:QueueMove("width:200;height:150", .03, 0);
	SLOT.ACTIVATION2:QueueParam("alpha", .3, 0, 0);
	SLOT.ACTIVATION2:QueueParam("alpha", 0, 0.5, 0);
	SLOT.ACTIVATION2:QueueMove("width:100;height:50", 0, 0);
	SLOT.ACTIVATION2:QueueMove("width:500;height:250", .03, 0);
end

function Slot_RefreshCooldown(SLOT)
	local elapsed = 0;
	local abInfo = g_abilityInfo[SLOT.globalIndex];

	if (abInfo and abInfo.state and abInfo.state.requirements) then
		local remainingCooldown = abInfo.state.requirements.remainingCooldown;
		local totalCooldown = abInfo.state.requirements.totalCooldown;
		local cooldownArc = SLOT.COOLDOWN_ARC;
		if (totalCooldown and totalCooldown > 0) then
			--currently in cooldown
			if (not SLOT.inCooldown) then
				Slot_EnterCooldown(SLOT);
			else
				Slot_EnterCooldown(SLOT, 0);
			end
			if SLOT.cb_ExitCooldown then
				cancel_callback(SLOT.cb_ExitCooldown)
			end
			SLOT.cb_ExitCooldown = callback(Slot_ExitCooldown, SLOT, remainingCooldown);

			cooldownArc:Show(true);

			SetCooldownArc(cooldownArc, remainingCooldown, totalCooldown);
			StartCooldownText(SLOT, remainingCooldown);
		elseif (SLOT.inCooldown) then
			cooldownArc:Show(false);
			-- coming out of cooldown
			if (SLOT.cb_ExitCooldown) then
				execute_callback(SLOT.cb_ExitCooldown);
				StartCooldownText(SLOT, 0)
			else
				StartCooldownText(SLOT, 0)
				Slot_ExitCooldown(SLOT);
			end
		else
			-- just refresh the slot
			Slot_SetFocus(SLOT, SLOT.inFocus, SLOT_SELECT_DUR, true);
		end
		if SLOT.globalIndex < FIRST_CONSUMABLE_SLOT then
			if abInfo.state.requirements.chargeCount and abInfo.state.requirements.chargeCount >= 0 then
				SLOT.CONSUMABLE_COUNTER:SetText(abInfo.state.requirements.chargeCount)
			else
				SLOT.CONSUMABLE_COUNTER:SetText("")
			end
		end
	end
end

function SetCooldownArc(cooldownArc, remainingCooldown, totalCooldown)
	local pct = 1 - remainingCooldown / totalCooldown;
	if (pct < 1) then
		if (g_cooldownFillsArc) then
			-- as pct goes from 0-1, angle goes from 180 to -180 which is a 'fill'
			local angle = 180 - pct * 360;
			cooldownArc:SetParam("end-angle", angle);
			cooldownArc:ParamTo("end-angle", -180, remainingCooldown);
		else
			-- as pct goes from 0-1, angle should go from -180 to 180 instead
			local angle = -180 + pct * 360;
			cooldownArc:SetParam("end-angle", angle);
			cooldownArc:ParamTo("end-angle", 180, remainingCooldown);
		end
	end
end

function CancelCooldownTextCallbacks(SLOT)
	if (SLOT.cb_ShowCooldown) then
		cancel_callback(SLOT.cb_ShowCooldown);
		SLOT.cb_ShowCooldown = nil;
	end

	if (SLOT.cb_HideCooldown) then
		cancel_callback(SLOT.cb_HideCooldown);
		SLOT.cb_HideCooldown = nil;
	end
end

function StartCooldownText(SLOT, remainingCooldown)
	CancelCooldownTextCallbacks(SLOT);

	if remainingCooldown > 0 then
		SLOT.COOLDOWN_COUNTER:StartTimer(remainingCooldown, true);
	end

	--[[
	if (remainingCooldown > COOLDOWN_COUNTER_SHOW_SECONDS) then
		SLOT.COOLDOWN_COUNTER:ParamTo("alpha", 0, 0.1);
		SLOT.cb_ShowCooldown = callback(
			function()
				SLOT.COOLDOWN_COUNTER:ParamTo("alpha", 1, 0.1);
				SLOT.cb_ShowCooldown = nil;
				SLOT.cb_HideCooldown = callback(
					function()
						SLOT.COOLDOWN_COUNTER:ParamTo("alpha", 0, 0.1);
						SLOT.cb_HideCooldown = nil;
					end,
					nil, COOLDOWN_COUNTER_SHOW_SECONDS);
			end,
			nil, remainingCooldown - COOLDOWN_COUNTER_SHOW_SECONDS);
	else
	]]--
	if (remainingCooldown > 0) then
		SLOT.COOLDOWN_COUNTER:ParamTo("alpha", 1, 0.1);
		SLOT.cb_HideCooldown = callback(
			function()
				SLOT.COOLDOWN_COUNTER:ParamTo("alpha", 0, 0.1);
				SLOT.cb_HideCooldown = nil;
			end,
			nil, remainingCooldown);
	else
		SLOT.COOLDOWN_COUNTER:ParamTo("alpha", 0, 0.1);
	end
end

function Slot_EnterCooldown(SLOT, aniDur)
	if (not aniDur) then
		-- seconds per frame
		aniDur = 1/30;
	end
	SLOT.inCooldown = true;
end

function Slot_ExitCooldown(SLOT)
	SLOT.cb_ExitCooldown = nil;
	local cooldownArc = SLOT.COOLDOWN_ARC;
	cooldownArc:Show(false);
end

function Slot_SetFocus(SLOT, focus, dur, forced)
	-- PSTODO: needed?
end

function SetupSimpleModel(modelObj, parentAnchor, color)
	if (modelObj) then
		modelObj.SCENEOBJECT:SetSortOrder(15); -- to stop flickering.

		if (parentAnchor) then
			modelObj.SCENEOBJECT:GetAnchor():SetParent(parentAnchor);
		end

		if (color) then
			modelObj.SCENEOBJECT:SetParam("tint", color);
		end
	end
end

function SetSimpleModelColor(modelObj, color)
	modelObj.SCENEOBJECT:SetParam("tint", color);
end

function SetupPercentShadedModel(modelObj, parentAnchor, onColor, offColor, stripes, initialPercent)
	if (modelObj) then
		modelObj.SCENEOBJECT:SetSortOrder(15); -- to stop flickering.

		if (parentAnchor) then
			modelObj.SCENEOBJECT:GetAnchor():SetParent(parentAnchor);
		end

		if (onColor) then
			modelObj.ColorOnId = modelObj.SCENEOBJECT:GetShaderParamId("ColorOn", "vec3");
			modelObj.SCENEOBJECT:SetShaderParam(modelObj.ColorOnId, onColor:toModelShaderParameter());
		end

		if (offColor) then
			modelObj.ColorOffId = modelObj.SCENEOBJECT:GetShaderParamId("ColorOff", "vec3");
			modelObj.SCENEOBJECT:SetShaderParam(modelObj.ColorOffId, offColor:toModelShaderParameter());
		end

		if (stripes) then
			modelObj.Stripes = modelObj.SCENEOBJECT:GetShaderParamId("Stripes", "float");
			modelObj.SCENEOBJECT:SetShaderParam(modelObj.Stripes, {x=stripes});
		end

		if (initialPercent) then
			modelObj.PercentId = modelObj.SCENEOBJECT:GetShaderParamId("Percent", "yAxisPercent");
			modelObj.SCENEOBJECT:SetShaderParam(modelObj.PercentId, initialPercent);
		end
	end
end

function SetShadedModelColor(model, shadedModelColor)
	model.SCENEOBJECT:SetShaderParam(model.ColorOnId, shadedModelColor:toModelShaderParameter());
end

function SetModelPercent(modelObj, percent)
	modelObj.SCENEOBJECT:SetShaderParam(modelObj.PercentId, {x=1-percent});
end

function CreateSceneObject(artName, instanceName)
	local model = Component.CreateSceneObject(artName, instanceName, "world");

	table.insert(g_SceneObjectsToRemove, model);

	return model
end

function CreateTrackingFrame(name)
	local FRAME = Component.CreateFrame("TrackingFrame", name);
	FRAME:GetAnchor():SetScene("world");
	return FRAME;
end

function UnloadSceneObjects()
	local numSceneObjectsToRemove = #g_SceneObjectsToRemove;

	for i = 1, numSceneObjectsToRemove do
		Component.RemoveSceneObject(table.remove(g_SceneObjectsToRemove));
	end
end

function SlotEmpty(globalIndex)
	return g_abilityInfo[globalIndex] == nil;
end

function IsValidSlotIndex(globalIndex)
	return globalIndex and globalIndex >= FIRST_ABILITY_SLOT and globalIndex <= LastConsumableIndex()
end

function IsConsumableSlot(globalIndex)
	return (globalIndex >= FirstConsumableIndex() and globalIndex <= LastConsumableIndex());
end

function SetHKMUnknown()
	g_HKMIndex = nil;
end

function CheckAbilityForHKM(abiInfo, globalIndex)
	if (abiInfo and abiInfo.isHKM) then
		g_HKMIndex = globalIndex;
	end
end

function IsHKMSlot(globalIndex)
	return globalIndex == g_HKMIndex;
end

function IsHKMReady()
	return g_SuperBarCharge >= BASICALLY_ONE_HUNDRED;
end

function FirstConsumableIndex()
	return FIRST_CONSUMABLE_SLOT;
end

function LastConsumableIndex()
	return FIRST_CONSUMABLE_SLOT + NUM_CONSUMABLE_SLOTS - 1;
end

function ConsumableIndexFromGlobal(globalIndex)
	return globalIndex - FirstConsumableIndex() + 1;
end

function GlobalIndexFromConsumable(consumableIndex)
	return consumableIndex + FirstConsumableIndex() - 1;
end

function FormTooltipForSlot(index)
	local abiInfo = g_abilityInfo[index];

	local tooltip = nil;

	if (abiInfo) then
		local epsilon = 0.09; -- value to drop trailing 0s with
		local cooldown;

		-- Two versions:
		-- 1) integer truncation when no decimal (commented if change needed in future)
		-- 2) rounding to nearest tenth of second

		-- This version performs integer truncation on the cooldown value.
		--if(math.abs(math.floor(abiInfo.cooldown) - abiInfo.cooldown) < epsilon) then
		--	cooldown = unicode.format("%d", abiInfo.cooldown);
		--else
		--	cooldown = unicode.format("%.1f", abiInfo.cooldown);
		--end

		-- This version rounds the cooldown value to the nearest tenth of a second.
		local roundedCooldown = 0.1 * (math.floor((abiInfo.cooldown + 0.05) * 10));
		if(math.abs(math.floor(roundedCooldown) - roundedCooldown) < epsilon) then
			cooldown = unicode.format("%d", roundedCooldown);
		else
			cooldown = unicode.format("%.1f", roundedCooldown);
		end

		local cooldownText = Component.LookupText("ABILITY_TOOLTIP_COOLDOWN", cooldown);
		local description;

		if (abiInfo and abiInfo.itemInfo) then
			description = abiInfo.itemInfo.description;
		elseif (abiInfo) then
			description = abiInfo.description;
		end

		local name = nil;

		if (abiInfo.itemInfo and abiInfo.itemInfo.name) then
			name = abiInfo.itemInfo.name;
		else
			name = abiInfo.name;
		end

		-- HKM abilities did not appear to have cooldowns, so no cooldown text for them
		if (IsHKMSlot(index)) then
			tooltip = name.."\n\n"..description;
		else
			tooltip = name.."  ("..cooldownText..")\n\n"..description;
		end
	end

	return tooltip;
end


function SwapConsumables(index1, index2)
	if (index1 and index2 and index1 ~= index2) then
		local item1 = ItemSdbIdFromSlotIndex(index1);
		local item2 = ItemSdbIdFromSlotIndex(index2);

		if (item1 and item2) then
			TryConsumableDrop(item1, index2);
			TryConsumableDrop(item2, index1);
		elseif (item1) then
			TryConsumableDrop(item1, index2);
			ClearConsumableSlot(ConsumableIndexFromGlobal(index1));
		elseif (item2) then
			TryConsumableDrop(item2, index1);
			ClearConsumableSlot(ConsumableIndexFromGlobal(index2));
		end
	end
end

function ClearConsumableSlot(consumableIndex)
	Player.SlotTech(nil, nil, consumableIndex);
	UpdateCalldownSettings(consumableIndex, INVALID_ABILITY_ID);
end

-- PSTODO: this is identical to the code doing the same thing in Mainframe.lua. Should probably move both out into a lib
-- or accomplish this by one messaging the other
function TryConsumableDrop(itemTypeId, globalIndex)
	itemTypeId = tonumber(itemTypeId);

	-- in the process of getting index-enabled
	local consumableIndex = ConsumableIndexFromGlobal(globalIndex);

	local consumables = Player.GetConsumableItems()
	for i, item in ipairs(consumables) do
		if tonumber(item.itemTypeId) == itemTypeId then
			Player.SlotTech(item.itemId, item.itemTypeId, consumableIndex);
			break;
		end
	end
end

function Brightify(colorObj, scale)
	local brightened = Colors.Create("#000000");

	brightened.r = colorObj.r * scale;
	brightened.g = colorObj.g * scale;
	brightened.b = colorObj.b * scale;

	return brightened;
end

function ModelBrightness(colorObj)
	return Brightify(colorObj, BRIGHTIFY_SCALE);
end

function LoadSettings()
	g_calldownSettings = Component.GetSetting("calldowns") or {};

	if (g_playerGUID) then
		if (g_calldownSettings[g_playerGUID] == nil) then
			g_calldownSettings[g_playerGUID] = {};

			-- try to load this with what was the not-per-character settings
			for i = 1, NUM_CONSUMABLE_SLOTS do
				g_calldownSettings[g_playerGUID][i] = g_calldownSettings[i];
				g_calldownSettings[i] = nil;
			end
		end
	else
		warn("3dActionBar.LoadSettings() failed (g_playerGUID == nil)")
	end
end

function UpdateCalldownSettings(consumableIndex, itemSdbId)
	if (g_calldownSettings and g_playerGUID) then
		g_calldownSettings[g_playerGUID][consumableIndex] = itemSdbId;
		SaveSettings();
	else
		warn("3dActionBar.UpdateCalldownSettings() failed (g_calldownSettings == nil or g_playerGUID == nil)");
	end
end

function SlotPendingCalldowns()
	if (g_calldownSettings and g_playerGUID) then
		for i = 1, NUM_CONSUMABLE_SLOTS do
			if (g_calldownSettings[g_playerGUID][i] ~= INVALID_ABILITY_ID) then
				Player.SlotTech(nil, g_calldownSettings[g_playerGUID][i], i);
			end
		end
	end
end

function SaveSettings()
	-- ensure all indices are ints
	local my_calldowns = g_calldownSettings[g_playerGUID];
	if (my_calldowns) then
		local cpy = {};
		for i=1, NUM_CONSUMABLE_SLOTS do
			cpy[i] = my_calldowns[i] or INVALID_ABILITY_ID;
		end
		g_calldownSettings[g_playerGUID] = cpy;
	end

	Component.SaveSetting("calldowns", g_calldownSettings);
end

function Show(show)
	if (show) then
		g_BaseAnchor:ParamTo("translation", ONSCREEN_BASE_ANCHOR_POS, HIDE_SHOW_DUR);
		MAIN_GROUP:Show(true);
		g_muted = false;
	else
		g_BaseAnchor:ParamTo("translation", OFFSCREEN_BASE_ANCHOR_POS, HIDE_SHOW_DUR);
		MAIN_GROUP:Show(false);
		g_muted = true;
	end
end

function OnHudShow(show, dur)
	g_hiddenByHUDHiding = not show;
	_3DAB.Hide(true) -- hide the original 3DAB
	CheckHUDVisibility();
end

function OnVehicleMode(args)
	if args.fake == true then return end -- don't listen to events sent by the library
	_3DAB.Hide(true) -- hide the original 3DAB
	g_hiddenBecauseVehicle = args.show;

	CheckHUDVisibility();
end

function CheckHUDVisibility()
	local hidden = g_hiddenBecauseVehicle or g_hiddenByHUDHiding;
	Show(not hidden);
end

function OnMessage(args)
	if (args.type == "DEBUG_MODE") then
		Debug.EnableLogging(args.data)
	elseif (args.type == "SHOW_MOUSE_MODE_TIPS") then
		g_ShowMouseModeTips = args.data;
		OnInputModeChanged({mode=g_InputMode})
	elseif args.type == "__DISPLAY" then

	elseif args.type == "ANCHOR_X" then
		ONSCREEN_BASE_ANCHOR_POS.x = tonumber(args.data)
		CheckHUDVisibility()
	elseif args.type == "ANCHOR_Y" then
		ONSCREEN_BASE_ANCHOR_POS.y = tonumber(args.data)
		CheckHUDVisibility()
	elseif args.type == "ANCHOR_Z" then
		ONSCREEN_BASE_ANCHOR_POS.z = tonumber(args.data)
		CheckHUDVisibility()
	elseif args.type == "TILT" then
		tilt = tonumber(args.data)
		g_BaseAnchor:SetParam("rotation", {axis={x=rotation_x,y=rotation_y,z=rotation_z}, angle=tilt})
	elseif args.type == "ROTATION_X" then
		rotation_x = tonumber(args.data)
		g_BaseAnchor:SetParam("rotation", {axis={x=rotation_x,y=rotation_y,z=rotation_z}, angle=tilt})
	elseif args.type == "ROTATION_Y" then
		rotation_y = tonumber(args.data)
		g_BaseAnchor:SetParam("rotation", {axis={x=rotation_x,y=rotation_y,z=rotation_z}, angle=tilt})
	elseif args.type == "ROTATION_Z" then
		rotation_z = tonumber(args.data)
		g_BaseAnchor:SetParam("rotation", {axis={x=rotation_x,y=rotation_y,z=rotation_z}, angle=tilt})
	elseif args.type == "SHOW_SELECTOR" then
		show_selector = (args.data == true)
		OnAbilitySelected()
	end
end

function ShowMouseModeHintCallback()
	MOUSE_MODE_HINTS.GROUP:ParamTo("alpha", 1, HINT_FADE_IN_DELAY_SECONDS);
	cb_FlashMouseModeHint = callback(HideMouseModeHintCallback, nil, HINT_SHOW_DELAY_SECONDS + HINT_FADE_IN_DELAY_SECONDS);
end

function HideMouseModeHintCallback()
	MOUSE_MODE_HINTS.GROUP:ParamTo("alpha", 0.5, HINT_FADE_OUT_DELAY_SECONDS);
	cb_FlashMouseModeHint = callback(ShowMouseModeHintCallback, nil, HINT_HIDE_DELAY_SECONDS + HINT_FADE_OUT_DELAY_SECONDS);
end

function UpdateLocks(is_showing)
	for i = 1,4 do
		if IsSlotLocked(i) and is_showing then
			w_Slots[i].LOCK:GetChild("lock"):ParamTo("alpha", .225, HINT_FADE_IN_DELAY_SECONDS, "smooth")
			w_Slots[i].LOCK:GetChild("lock"):MoveTo("center-y:35%", HINT_FADE_IN_DELAY_SECONDS, "smooth")
			w_Slots[i].LOCK:GetChild("lock_text"):ParamTo("alpha", .4, HINT_FADE_IN_DELAY_SECONDS+.1, "smooth")
		elseif IsSlotLocked(i) then
			w_Slots[i].LOCK:GetChild("lock"):ParamTo("alpha", .15, HINT_FADE_IN_DELAY_SECONDS, "smooth")
			w_Slots[i].LOCK:GetChild("lock"):MoveTo("center-y:50%", HINT_FADE_IN_DELAY_SECONDS, "smooth")
			w_Slots[i].LOCK:GetChild("lock_text"):ParamTo("alpha", 0, HINT_FADE_IN_DELAY_SECONDS, "smooth")
		end
	end
end

function OnInputModeChanged(args)
	Tooltip.Show(false);
	g_InputMode = args.mode

	UpdateItemCooldownDisplay()
	if (args.mode == "cursor" and g_ShowMouseModeTips) then
		MOUSE_MODE_HINTS.GROUP:Show(true);
		MOUSE_MODE_HINTS.GROUP:SetParam("alpha", 1);
		if (cb_FlashMouseModeHint) then
			cancel_callback(cb_FlashMouseModeHint);
		end
		cb_FlashMouseModeHint = callback(HideMouseModeHintCallback, nil, HINT_SHOW_DELAY_SECONDS)
		UpdateLocks(true)
		ShowItemHotkeys(true)
	else
		MOUSE_MODE_HINTS.GROUP:Show(false);
		if (cb_FlashMouseModeHint) then
			cancel_callback(cb_FlashMouseModeHint);
			cb_FlashMouseModeHint = nil;
		end
		UpdateLocks(false)

		ShowItemHotkeys(false)

		-- this should cancel a drag in progress, right? false means drag canceled
		EndDragOperation(false);
	end
end

function OnLevelChanged()
	for i = 1,4 do
		w_Slots[i].LOCK:GetChild("lock"):ParamTo("alpha", math.min(tonumber(IsSlotLocked(i)), .15), HINT_FADE_IN_DELAY_SECONDS, "smooth")
		w_Slots[i].LOCK:GetChild("lock"):MoveTo("center-y:50%", HINT_FADE_IN_DELAY_SECONDS, "smooth")
		w_Slots[i].LOCK:GetChild("lock_text"):ParamTo("alpha", 0, HINT_FADE_IN_DELAY_SECONDS, "smooth")
	end
end

function EndDragOperation()
	Component.EndDragDrop(false);
end

function PlaySound(soundName)
	if (not g_muted) then
		System.PlaySound(soundName);
	end
end

function IsSlotLocked(index)
	return Player.GetLevel() < Game.RequiredLevelForSlot(AbilityIndex[index])
end

function OnUIEncounterMessage(args)
	if (args.messageName == "TUTORIAL_EVENT") then
		local paramTable = jsontotable(args.messageContents);

		if (paramTable) then
			if (paramTable.Event == "CALLDOWN_TRAINING_ATTRACT") then
				local dur = paramTable.Duration or 5;

				local attHost;

				if (IsValidSlotIndex(paramTable.Slot)) then
					attHost = w_Slots[tonumber(paramTable.Slot)]
				else
					attHost = w_Slots[FIRST_CONSUMABLE_SLOT];
				end

				if (attHost) then
					Attract.KickAttractor(attHost, ACTIONBAR_ATTRACTOR_SCALE);

					callback(function() Attract.HideAttractor(attHost) end, nil, dur);

					-- echo back for testing purposes
					Game.SendUIMessageToEncounter(args.encounterID, args.messageName, args.messageContents);
				end
			end
		end
	end
end







--- MED and AUX Icons Stuff below!

function ShowItemHotkeys(show)
	local alpha = show and 1 or 0
	MED_KEY:ParamTo("alpha", alpha, c_KeyFadeTime)
	AUX_KEY:ParamTo("alpha", alpha, c_KeyFadeTime)
end

function CreateItemCooldownFrame(parentAnchor)
	local FRAME = CreateTrackingFrame("ItemCooldownFrame")
	ITEM_COOLDOWN_GRP = Component.CreateWidget("ItemCooldowns", FRAME)

	MED_KEY = ITEM_COOLDOWN_GRP:GetChild("MedKey")
	AUX_KEY = ITEM_COOLDOWN_GRP:GetChild("AuxKey")

	local medcardgroup = ITEM_COOLDOWN_GRP:GetChild("MedCard")
	local auxcardgroup = ITEM_COOLDOWN_GRP:GetChild("AuxCard")

	MED_CARD = ItemCard.Create(medcardgroup:GetChild("CardHolder"))
	MED_CARD:SetSize(40)
	MED_CARD:HideBackPlate(true)

	AUX_CARD = ItemCard.Create(auxcardgroup:GetChild("CardHolder"))
	AUX_CARD:SetSize(40)
	AUX_CARD:HideBackPlate(true)

	ITEMWIDGETS.MED_COOLDOWNTIMER = medcardgroup:GetChild("cooldown_counter")
	ITEMWIDGETS.MED_COOLDOWNMASK = medcardgroup:GetChild("cooldown_mask")

	ITEMWIDGETS.AUX_COOLDOWNTIMER = auxcardgroup:GetChild("cooldown_counter")
	ITEMWIDGETS.AUX_COOLDOWNMASK = auxcardgroup:GetChild("cooldown_mask")

	FRAME:SetInteractable(true)
	FRAME:SetParam("cullalpha", 1)
	local left, top, width, height = FRAME:GetBounds()
	local frameWidth = 64
	local frameHeight = 64

	FRAME:SetBounds(-(frameWidth / 2), -(frameHeight / 2), frameWidth, frameHeight)

	local ANCHOR = FRAME:GetAnchor()
	ANCHOR:SetParent(parentAnchor)
	ANCHOR:SetParam("translation", {x = 0, y = 0, z = 0}) -- 0.25
	ANCHOR:SetParam("rotation", {axis={x=0,y=0,z=1}, angle=-c_TiltAngle})
end

function LoadItemCards()
	local loadout = Player.GetCurrentLoadout()
	if loadout then
		ITEM_COOLDOWN_GRP:Show(true)
		local medId = GetMedicalIdFromLoadout(loadout)
		UpdateCardAndKey(MED_CARD, MED_KEY, medId)

		local auxId = GetAuxiliaryIdFromLoadout(loadout)
		UpdateCardAndKey(AUX_CARD, AUX_KEY, auxId)
	else
		ITEM_COOLDOWN_GRP:Show(false)
	end
end

function UpdateCardAndKey(CARD, KEY, itemId)
	local showCard = itemId ~= nil
	if showCard then
		CARD:LoadItem(itemId)
	end
	CARD:Show(showCard)
	KEY:Show(showCard)
end

function UpdateKeyBindings()
	SetKeybindTexture(MED_KEY, "Combat", "MedicalSystem")
	SetKeybindTexture(AUX_KEY, "Combat", "MeleeAttack")
end

function SetKeybindTexture(WIDGET, category, name)
	local catBindings = System.GetKeyBindings(category, false) or {}
	local keybindings = catBindings[name]--[1].keycode
	if keybindings and #keybindings > 0 then
		local keystring = keybindings[1].keycode
		local texture, region = InputIcon.GetTexture(keystring)
		if texture then
			WIDGET:SetTexture(texture)
			WIDGET:SetRegion(region)
		end
	else
		warn("Could not find keybinding for category "..tostring(category).." and binding "..tostring(name))
	end
end

function UpdateItemCooldownDisplay()
	local showcooldowns = (g_InputMode ~= "cursor")--Should hide cooldowns when in mouse mode

	if AUX_CARD then
		local auxstate = Player.GetAbilityState(AUX_CARD.itemInfo.abilityId)
		local auxcd = auxstate.requirements.remainingCooldown
		if showcooldowns and auxcd and auxcd > 0 then
			local auxmaxcd = auxstate.requirements.totalCooldown
			ITEMWIDGETS.AUX_COOLDOWNTIMER:ParamTo("alpha",1,.1)
			ITEMWIDGETS.AUX_COOLDOWNTIMER:StartTimer(auxcd,true)-->?
			AUX_CARD:Disable()
			--[[Temporarily disabled, since the shader doesn't work yet
			ITEMWIDGETS.AUX_COOLDOWNTIMER:Show()
			ITEMWIDGETS.AUX_COOLDOWNMASK:SetParam("percent",auxcd/auxmaxcd)
			ITEMWIDGETS.AUX_COOLDOWNMASK:ParamTo("percent",0,auxcd)
			--]]
			callback(UpdateItemCooldownDisplay,nil,auxcd)
		else
			ITEMWIDGETS.AUX_COOLDOWNTIMER:ParamTo("alpha",0,.1)
			AUX_CARD:Enable()
		end
	end

	if MED_CARD then
		local medstate = Player.GetAbilityState(MED_CARD.itemInfo.abilityId)
		local medcd = medstate.requirements.remainingCooldown
		if showcooldowns and medcd and medcd > 0.1 then
			local medmaxcd = medstate.requirements.totalCooldown
			ITEMWIDGETS.MED_COOLDOWNTIMER:ParamTo("alpha",1,.1)
			ITEMWIDGETS.MED_COOLDOWNTIMER:StartTimer(medcd,true)-->?
			MED_CARD:Disable()
			--[[Temporarily disabled, since the shader doesn't work yet
			ITEMWIDGETS.AUX_COOLDOWNMASK:Show()
			ITEMWIDGETS.AUX_COOLDOWNMASK:SetParam("percent",medcd/medmaxcd)
			ITEMWIDGETS.AUX_COOLDOWNMASK:ParamTo("percent",0,medcd)
			--]]
			callback(UpdateItemCooldownDisplay,nil,medcd)
		else
			ITEMWIDGETS.MED_COOLDOWNTIMER:ParamTo("alpha",0,.1)
			MED_CARD:Enable()
		end
	end
end

function GetMedicalIdFromLoadout(loadout)
	local medicalSlotId = 123

	return FindSlotItemIdInBackpack(loadout, medicalSlotId)
end

function GetAuxiliaryIdFromLoadout(loadout)
	local auxSlotId = 122

	return FindSlotItemIdInBackpack(loadout, auxSlotId)
end

function FindSlotItemIdInBackpack(loadout, slotId)
	if not loadout or not loadout.modules or not loadout.modules.backpack then
		return nil
	end
	for _, moduleInfo in ipairs(loadout.modules.backpack) do
		if moduleInfo.slot_type_id == slotId then
			return moduleInfo.item_sdb_id
		end
	end

	return nil
end